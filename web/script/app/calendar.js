/* global confirm_info */
/* global msg_error */
/* global moment */
/* global $ */

function cdate(calendar){
  return moment($(calendar).data("curdate"), "YYYY/MM/DD");
}

function swap_calendar(){
  var calendar = $("#calendar");
  var calendar_week = $("#calendar_week")

  if(calendar.is(":visible")){
    calendar_week.data("curdate", calendar.data("curdate"))
    calendar.hide();
    calendar_week.show();
    reload_calendar_week(calendar_week);
    reload_calendar_activite_week(calendar_week);
  }else{
    calendar.data("curdate", calendar_week.data("curdate"))
    calendar_week.hide();
    calendar.show();
    reload_calendar_month(calendar);
    reload_calendar_activite_month(calendar);
  }
}

function header_week_title(calendar){
  var t1 = cdate(calendar).startOf('week');
  var t2 = cdate(calendar).endOf('week');
  if(t1.year() != t2.year()){
    return t1.format('D MMM YYYY')+' au '+t2.format('D MMM YYYY')
  }else{
    return t1.format('D MMM')+' au '+t2.format('D MMM YYYY')
  }
}

function draw_header_month(calendar, title, btn_left, btn_right){
  title.html(cdate(calendar).format('MMMM YYYY'));
  
  btn_left.on('click', function(){
    var d = moment($(calendar).data("curdate"), "YYYY/MM/DD");
    d.subtract(1, 'month');
    $(calendar).data("curdate", d.format("YYYY/MM/DD"));
    reload_calendar_month(calendar);
    reload_calendar_activite_month(calendar);
  })
  
  btn_right.on('click', function(){
    var d = moment($(calendar).data("curdate"), "YYYY/MM/DD");
    d.add(1, 'month');
    $(calendar).data("curdate", d.format("YYYY/MM/DD"));
    reload_calendar_month(calendar);
    reload_calendar_activite_month(calendar);
  })
}

function draw_header_week(calendar, title, btn_left, btn_right){
  title.html(header_week_title(calendar));
  
  btn_left.on('click', function(){
    var d = moment($(calendar).data("curdate"), "YYYY/MM/DD");
    d.subtract(1, 'week');
    $(calendar).data("curdate", d.format("YYYY/MM/DD"));
    reload_calendar_week(calendar);
    reload_calendar_activite_week(calendar);
  })
  
  btn_right.on('click', function(){
    var d = moment($(calendar).data("curdate"), "YYYY/MM/DD");
    d.add(1, 'week');
    $(calendar).data("curdate", d.format("YYYY/MM/DD"));
    reload_calendar_week(calendar);
    reload_calendar_activite_week(calendar);
  })
  
}

function draw_header(calendar, view){
  var head = $(document.createElement('div'));
  var btn_left = $(document.createElement('div'));
  var btn_right = $(document.createElement('div'));
  var btn_options = $(document.createElement('div'));
  var btn_pad = $(document.createElement('div'));
  var title = $(document.createElement('div'));
  
  head.addClass("calendar-head");
  title.addClass("calendar-title");
  
  btn_left.addClass("calendar-head-btn");
  btn_right.addClass("calendar-head-btn");
  btn_options.addClass("calendar-head-btn");
  btn_pad.addClass("calendar-head-btn padding");
  btn_left.append($("<i class=\"fa fa-chevron-left\"></i>"))
  btn_right.append($("<i class=\"fa fa-chevron-right\"></i>"))
  btn_options.append($("<i class=\"fa fa-table\"></i>"))
  btn_pad.append($("<i style='display: None;' class=\"fa fa-chevron-right\"></i>"))
  
  btn_options.on('click', swap_calendar);
  
  if(view == 'month')
    draw_header_month(calendar, title, btn_left, btn_right)
  else if(view == 'week')
    draw_header_week(calendar, title, btn_left, btn_right)
  
  head.append(btn_left); 
  head.append(btn_options); 
  head.append(title);
  head.append(btn_pad);
  head.append(btn_right);
  calendar.append(head);
}

function calendar_days(calendar){
  var days = [];
  var xday = cdate(calendar).startOf('month').startOf('day');
  
  xday.subtract(xday.day(), 'day');

  while(days.length < 6*7){
    days.push(moment(xday));
    xday.add(1, 'day');
  }
  
  return days;
}

function draw_dates(calendar, view){
  // Dessine le conteneur pour les dates
  var body = $(document.createElement('div'));
  body.addClass("calendar-body");
  
  if(view == 'month')
    draw_dates_month(calendar, body)
  else if(view == 'week')
    draw_dates_week(calendar, body)
  
  calendar.append(body);
}

function draw_dates_week(calendar, body){
  var N_COL = 7;
  var xday = moment(cdate(calendar)).startOf('week');
  
  // Dessine la première colonne pour les jours de la semaine
  for (var i=0; i < N_COL; ++i) {
    var col_head = $(document.createElement('div'));
    col_head.addClass("calendar-col-head");
    col_head.html(xday.format('ddd DD/MM'));
    xday.add(1, 'day');
    body.append(col_head);
  }
  
  body.append($("</br>"));
  xday.subtract(7, 'day');
 
  // Dessine les cases pour les activités
  for (var i=0; i < N_COL; ++i) {
    var date = $(document.createElement('div'));
    date.addClass("calendar-date calendar-date-week");
    
    if (xday.dayOfYear() == moment().dayOfYear())
        date.addClass("calendar-date-current");
    if (xday.day() == 6)
        date.addClass("calendar-date-limit-right");
    
    date.data('date', moment(xday));
    body.append(date);
    xday.add(1, 'day');
  }

}

function draw_dates_month(calendar, body){
  var N_ROW = 6;
  var N_COL = 7;
  var days = calendar_days(calendar);
 
  var xday = cdate(calendar).startOf('month');
  var yday = cdate(calendar).endOf('month');
  var zday = moment();
  
  // Dessine la première colonne pour les jours de la semaine
  var days_of_week = 'Dimanche_Lundi_Mardi_Mercredi_Jeudi_Vendredi_Samedi'.split('_');
  for (var i=0; i < N_COL; ++i) {
    var col_head = $(document.createElement('div'));
    col_head.addClass("calendar-col-head");
    col_head.html(days_of_week[i]);
    body.append(col_head);
  }
  body.append($("</br>"));
 
  // Dessine les cases de dates
  for (var i=0; i < N_ROW; ++i) {
    for (var j=0; j < N_COL; ++j) {
      var date = $(document.createElement('div'));
      var date_text = $(document.createElement('div'));
      var date_date = days.shift();
      
      date.addClass("calendar-date");
      if (date_date < xday || date_date > yday)
        date.addClass("calendar-date-inactive");
      if (date_date.day() == 6)
        date.addClass("calendar-date-limit-right");
      if (date_date.dayOfYear() == zday.dayOfYear())
        date.addClass("calendar-date-current");
            
      date.data('date', date_date);
      date_text.html(date_date.date());
      date.append(date_text);
      body.append(date);
    }
    body.append($("</br>"));
  }
  
}

function draw_footer(calendar){
  var foot = $(document.createElement('div'));
  foot.addClass("calendar-footer");
  calendar.append(foot);
}

// Recharge le calendrier (pour un changement de date par exemple)
function reload_calendar_month(calendar){
  var calendar_ = $(calendar);
  var title = calendar_.find(".calendar-title");
  var date_divs = calendar_.find(".calendar-date");
  var days = calendar_days(calendar);
  title.html(cdate(calendar).format('MMMM YYYY'));
  
  var xday = cdate(calendar).startOf('month');
  var yday = cdate(calendar).endOf('month');
  var zday = moment();

  date_divs.each(function(){
    var date = $(this);
    var date_date = days.shift();
    
    if (date_date < xday || date_date > yday)
      date.addClass("calendar-date-inactive");
    else
      date.removeClass("calendar-date-inactive");

    if (date_date.dayOfYear() == zday.dayOfYear() && date_date.year() == zday.year())
      date.addClass("calendar-date-current");
    else
      date.removeClass("calendar-date-current");
    
    date.data('date', date_date);
    date.children('div').html(date_date.date());
  });
  
}

function reload_calendar_week(calendar){
  var calendar_ = $(calendar);
  var xday = moment(cdate(calendar_)).startOf('week');
  var zday = moment();
  var title = calendar_.find(".calendar-title");
  var date_divs = calendar_.find(".calendar-date");
 
  title.html(header_week_title(calendar));
  
  //Header du calendrier
  calendar_.find('.calendar-col-head').each(function(){
    $(this).html(xday.format('ddd DD/MM'));
    xday.add(1, 'day');
  });
  
  xday.subtract(7, 'day');
  
  // Recharge les dates
  date_divs.each(function(){
    var date = $(this);
    
    if (xday.dayOfYear() == zday.dayOfYear() && xday.year() == zday.year())
      date.addClass("calendar-date-current");
    else
      date.removeClass("calendar-date-current");
    
    date.data('date', moment(xday));
    xday.add(1, 'day');
  }); 
 
}

// Retourne la prochaine activité
var next_activite = function(activites, index){
  if (activites[index] !== undefined)
    return moment(activites[index].date.date, 'YYYY-MM-DD HH:mm:ss')
  else
    return null;
}

//Crée un nouvel event
var new_event = function(date_div, text, status, link){
  var evt = $("<div><a></a></div>");
  var evt_link = evt.children().first();
  evt.addClass('calendar-event');
  
  evt.hide();
  
  evt_link.width(date_div.width());
  evt_link.html(text);
  evt_link.attr("href", link)
  
  if(status === 'green')
    evt.addClass('green-event');
  else if(status === 'yellow')
    evt.addClass('yellow-event');
  else
    evt.addClass('red-event');
  
  date_div.append(evt);
  evt.fadeIn();
  
  return evt;
}

//Crée un nouvel event (vue semaine)
var new_event_week = function(date_div, activite, link){
  var evt = $("<div><a><div></div><div></div><div></div></a></div>");
  var evt_link = evt.children().first();
  var text = evt_link.children().first();
  evt.addClass('calendar-event');
  
  evt.hide();
  
  evt_link.width(date_div.width());
  evt_link.attr("href", link);
  
  if(activite.status === 'green')
    evt.addClass('green-event');
  else if(activite.status === 'yellow')
    evt.addClass('yellow-event');
  else
    evt.addClass('red-event');

  var xdate = moment(activite.date.date, 'YYYY-MM-DD HH:mm:ss').format('HH:mm');
  if(xdate === "00:00")
    text = text.html(" - ");
  else
    text = text.html(xdate);
    
  text = text.next().html(activite.equipe);

  if(activite.division === null || activite.division.length == 0)
    text.next().html(" - ");
  else
    text.next().html(activite.division);
  
  
  date_div.append(evt);
  evt.fadeIn();
  
  return evt;
}

function reload_calendar_activite_month(calendar){
  var activites = calendar.data('activites');
  if (activites === undefined) 
    return false;
  
  var date_divs = calendar.find(".calendar-date");
  var count = 0;
  
  date_divs.each(function(){
    var date_div = $(this);
    var date = date_div.data('date');
    var tomorrow = moment(date).add(1, 'days');
    
    // Supprime les vielles activités
    date_div.children('.calendar-event').remove();

    // Skip les activité qui ne sont pas dans la journée
    while(next_activite(activites, count) !== null && date.isAfter(next_activite(activites, count)))
      count += 1;

    // Charge les activité de la journée
    var date_div_activities = [];
    while(next_activite(activites, count) !== null && next_activite(activites, count) < tomorrow){
      date_div_activities.push(activites[count]);
      count += 1;
    }
    
    // Dessine les activité de la journée
    if(date_div_activities.length <= 3){
      $.each(date_div_activities, function(){ new_event(date_div, this.equipe, this.status, "/activites/"+this.id+"?calendar=true") });
    }else{
      $.each(date_div_activities.slice(0,2), function(){ new_event(date_div, this.equipe, this.status, "/activites/"+this.id+"?calendar=true") });
      new_event(date_div, (date_div_activities.length - 2)+" de plus...", "javascript:;").on('click', 
        function(){ calendar.data("curdate", date);  swap_calendar();}
      )
    }
  });
  
  return true;
}

function reload_calendar_activite_week(calendar){
  var activites = calendar.data('activites');
  if (activites === undefined) 
    return false;
  
  var date_divs = calendar.find(".calendar-date");
  var count = 0;
  
  date_divs.each(function(){
    var date_div = $(this);
    var date = date_div.data('date');
    var tomorrow = moment(date).add(1, 'days');
    
    // Supprime les vielles activités
    date_div.children('.calendar-event').remove();
    
    // Skip les activité qui ne sont pas dans la journée
    while(next_activite(activites, count) !== null && date.isAfter(next_activite(activites, count)))
      count += 1;
    
    // Charge et dessine les activité de la journée
    while(next_activite(activites, count) !== null && next_activite(activites, count) < tomorrow){
      var a = activites[count];
      new_event_week(date_div, a, "/activites/"+a.id);
      count += 1;
    }
    
  });
  
}

function create_activite(e){
  if (e.target !== this)
    return;
  
  var date = $(this).data('date');
  var now = moment().startOf('day');
  if(date.isBefore(now)){
    msg_error('Erreur! Il est impossible de créer une activité à une date antérieur à la date d\'aujourd\'hui!');
  }else{
    confirm_info("Voulez-vous vraiment créer une activité le "+ date.format('D MMMM YYYY')+"?",
      function(){
        var date_param = $.param({'date': date.format('YYYY/MM/DD HH:mm'), 'calendar': true})
        document.location.href = "/activites/new?"+date_param
    });
    
  }
}

$.fn.init_calendar = function() {
  moment.locale('fr');
  draw_header(this, 'month');
  draw_dates(this, 'month');
  draw_footer(this);
  
  $(".calendar-date").dblclick(create_activite);
}

$.fn.init_calendar_week = function() {
  moment.locale('fr');
  draw_header(this, 'week');
  draw_dates(this, 'week');
  draw_footer(this);

  $(".calendar-date").dblclick(create_activite);
}

$.fn.fill_calendar = function(data) {
  this.data('activites', data);
  reload_calendar_activite_month(this);
}

$.fn.fill_calendar_week = function(data) {
  this.data('activites', data);
  reload_calendar_activite_week(this);
}

