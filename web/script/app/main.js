/* global $ */

// Procédure de génération de message "flash"
function flash_base(msg, _class){
  var flash_td = $("#tab-general").children("."+_class).first();
  if(flash_td.length == 0){
    flash_td = $(document.createElement('div'));
    flash_td.addClass(_class);
    flash_td.insertBefore($("#tab-general").children().first());
  }
  flash_td.html(msg);
}

function flash(msg){ flash_base(msg, "flash"); }
function flash_err(msg){ flash_base(msg, "error-flash"); }

// Procédure de génération de message box
function modal_mini_base(msg, _class, cb, cb2){
  var modal_mini_msg = $("#modal-mini .modal-body span");
  var modal_mini_foot = $("#modal-mini .modal-footer");
  var modal_content = $("#modal-mini .modal-content");
  modal_content.removeClass('modal-error modal-default modal-warning');
  modal_content.addClass(_class);
 
  if(_class === 'modal-error'){
    modal_mini_foot.html('<div type="button" data-dismiss="modal" class="btn btn-danger">Ok</div>')
  }else if(_class === 'modal-warning' || _class === 'modal-default'){
    var cancel = $('<div type="button" data-dismiss="modal" class="btn btn-primary">Non</div>')
    var accept = $('<div type="button" data-dismiss="modal" class="btn">Oui</div>')

    if(_class === 'modal-warning'){ accept.addClass('btn-warning') }
    else if(_class === 'modal-default'){ accept.addClass('btn-success') }

    accept.on('click', cb);
    cancel.on('click', cb2);
    modal_mini_foot.html("")
    modal_mini_foot.append(cancel);
    modal_mini_foot.append(accept);
  }
 
  modal_mini_msg.html(msg);
  
  $("#modal-mini").modal();
}

function msg_error(msg){ modal_mini_base(msg, 'modal-error', null) }
function confirm_warning(msg, cb, cb2){ modal_mini_base(msg, 'modal-warning', cb, cb2) }
function confirm_info(msg, cb){ modal_mini_base(msg, 'modal-default', cb, function(){}) }

// Génère une fonction qui retournera un action pour supprimer un objet
function build_delete_button_action(entity, msg){
  return function(){
    var btn = $(this);
    if (btn.data('locked') === true)
      return;
    else
      btn.data('locked', true);
    
    var id = btn.data("id");
   
    confirm_warning(msg, function(){ 
      var post = $.post("/"+entity+"/"+id+"/delete.json");
      
      post.done(function(d){ 
        btn.closest("tr").fadeOut({'duration': 500});
        flash(d['success']);
      });
      
      post.fail(function(d){ 
        flash_err('Erreur: '+JSON.parse(d["responseText"])['error']);
        btn.find("i").toggleClass("fa-close fa-refresh fa-spin");
      });
      
      btn.find("i").toggleClass("fa-close fa-refresh fa-spin");
    },
    function(){
      btn.data('locked', false);
    });
    
  }
}

// Retourne on bouton de modification
function edit_button(entity, id){
  return "<div class='pull-right off-right'> <a class='btn btn-primary' href='/"+entity+"/"+id+"'> <i class='fa fa-pencil'></i></a></div>";
}

// Retourne on bouton de suppression
// L'action de suppression n'est pas automatiquement appliqué
// Pour cela, il faut utiliser 'build_delete_button_action' avec un 'on'.
function kill_button(id){
  return "<div class='pull-right'> <a data-id='"+id+"' class='btn btn-danger btn-hide' href='javascript:;'>  <i class='fa fa-close'></i></a></div>";
}

// Pagination

// Configure la pagination de la page. Cette fonction doit être appelée depuis le JS local
// de la page. Entity est le nom le l'entity, search_fn est la fonction qui génère les entrée.
function init_paginate(search_fn){ 
  $('.pagination')
    .data('search_fn', search_fn)
}

// Recharge le contrôle de la pagination
function update_pagination(){
  var page = $("#search").data('page');
  
  $(".pagination>li.previous, .pagination>li.next").removeClass('disabled');
  $('.pagination>li.n').removeClass('active');
  
  if(page==0) $(".pagination>li.previous").addClass('disabled');
  
  $(".pagination>li.n").each(function(){
     if($(this).data('page') == page){
        $(this).addClass('active');
        if($(this).next().hasClass('next')) $(".pagination>li.next").addClass('disabled');
     }
  });
}

// Pagination en cliquant sur les nombres
$('.pagination>li.n').on('click', function(){
  if($(this).hasClass('active')) return 
  
  var search_fn = $('.pagination').data('search_fn');
  var page = $(this).data('page');
  if( search_fn === undefined)
    alert('ERREUR: init_paginate n\'a pas été appelé dans le JS local.');
  else{
    $("#search").data('page', page);
    update_pagination();
    search_fn();
  }
})

// Pagination en cliquant sur les flèches
$(".pagination>li.previous").on('click', function(){
  if($(this).hasClass('disabled')) return 
  
  var search_fn = $('.pagination').data('search_fn');
  var page = $("#search").data('page');
  if( search_fn === undefined)
    alert('ERREUR: init_paginate n\'a pas été appelé dans le JS local.');
  else{
    $("#search").data('page', page-1);
    update_pagination();
    search_fn();
  }
})

$(".pagination>li.next").on('click', function(){
  if($(this).hasClass('disabled')) return 
  
  var search_fn = $('.pagination').data('search_fn');
  var page = $("#search").data('page');
  if( search_fn === undefined)
    alert('ERREUR: init_paginate n\'a pas été appelé dans le JS local.');
  else{
    $("#search").data('page', page+1);
    update_pagination();
    search_fn();
  }
})

$.fn.init_statut_editor = function() {
  var main = $(this);
  data = JSON.parse(main.val());
  
  var main_status = $("<select><option value='green'>Vert</option><option value='yellow'>Jaune</option><option value='red'>Rouge</option></select>");
  status_info = $("<textarea rows='4' cols='50'></textarea>")
  main_status.attr('style', 'width: 200px;');
  main_status.addClass('form-control');
  main.hide();
  
  main_status.val(data['status']);
  if(data['details'] !== undefined){
    status_info.val(data['details']);
  }
  if(data['status'] === 'green'){
    status_info.hide();
  }
  
  main_status.on('change', function(){
    var data = JSON.parse(main.val());
    data['status'] = $(this).val();
    main.val(JSON.stringify(data));
    
    if($(this).val() !== 'green'){
      status_info.show()
    }else{
      status_info.hide()
    }
    
  });
  
  status_info.on('focusout', function(){
    var data = JSON.parse(main.val());
    data['details'] = $(this).val();
    main.val(JSON.stringify(data));
  });
  
  main.after(main_status);
  main_status.after(status_info);
}