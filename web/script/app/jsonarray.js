/* global $ */

function eval_values(main, parent){
  var rows = parent.find("tr");
  var obj = {};
  var count = 0;
  rows.each(function(){
    var value = $(this).find('input').val();
    if(value.length > 0)
      obj[count++] = $(this).find('input').val();
  });
  
  main.val(JSON.stringify(obj));
}

function json_or_empty(data){
  var j = {};
  try{
    j = JSON.parse(data);
  }catch(SyntaxError){}
  return j;
}

function td_wrap(item){
  var td = $('<td class="json-array-td"></td>');
  td.append(item);
  return td;
}

function remove_item(parent, remove_btn){
  var rows = parent.find("tr");
  if(rows.size() == 2)
    parent.find('.json-array-remove').prop('disabled', true);
  if(rows.size() > 1)
    rows.last().remove();
}

function add_item(main, parent, value, options){
  var tr = $("<tr class='json-array-tr'></tr>");
  var input = $('<input class="form-control" style="width: 140px;" type="text"></input>');
  input.val(value);
  
  tr.append(td_wrap(input));
  parent.children().first().append(tr);
  
  parent.find('.json-array-remove').prop('disabled', false);
  
  if(options['mask'] !== undefined)
    input.mask(options['mask'], {'placeholder': options['placeholder']});
  input.on('focusout', function(){eval_values(main, parent)})
}

function add_buttons(main, parent, options){
  var tr = parent.find("tr").first();
  var add_btn = $("<button type='button' class='btn btn-success json-array-add'>  <i class='fa fa-plus'></i></button>");
  var remove_btn = $("<button type='button' class='btn btn-danger json-array-remove'>  <i class='fa fa-minus'></i></button>");
 
  add_btn.on('click', function(){add_item(main, parent, "", options);});
  remove_btn.on('click', function(){remove_item(parent);})
  
  tr.append(td_wrap(add_btn));
  tr.append(td_wrap(remove_btn));
}

$.fn.init_jsonarray = function(options) {
  var main = $(this);
  var item_table = $("<table class='json_array_table'><tbody></tbody></table>");

  // Add the items
  var ok = false;
  $.each(json_or_empty(main.val()), function(){
    ok = true;
    add_item(main, item_table, this, options);
  });
  
  if(!ok) add_item(main, item_table, "", options);
  add_buttons(main, item_table, options);
  
  if(item_table.find('tr').size() == 1)
    item_table.find('.json-array-remove').prop('disabled', true);
  

  main.after(item_table);
}
