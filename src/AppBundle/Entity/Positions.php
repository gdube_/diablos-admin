<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Positions
 *
 * @ORM\Table(name="positions", indexes={@ORM\Index(name="id_sport", columns={"id_sport"})})
 * @ORM\Entity
 */
class Positions
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_position", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="position", type="string", length=50, nullable=false)
     */
    private $position;

    /**
     * @var \AppBundle\Entity\Sports
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Sports")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_sport", referencedColumnName="id_sport")
     * })
     */
    private $idSport;


}

