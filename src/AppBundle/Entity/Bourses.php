<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Bourses
 *
 * @ORM\Table(name="bourses", indexes={@ORM\Index(name="id_joueur", columns={"id_joueur"})})
 * @ORM\Entity
 */
class Bourses
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_bourse", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var float
     *
     * @ORM\Column(name="montant", type="float", precision=8, scale=2, nullable=false)
     */
    private $montant;

    /**
     * @var string
     *
     * @ORM\Column(name="provenance", type="string", length=50, nullable=false)
     */
    private $provenance;

    /**
     * @var integer
     *
     * @ORM\Column(name="annee", type="integer", nullable=false)
     */
    private $annee;

    /**
     * @var \AppBundle\Entity\Joueurs
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Joueurs")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_joueur", referencedColumnName="id_joueur")
     * })
     */
    private $idJoueur;


}

