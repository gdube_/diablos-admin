<?php

namespace AppBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;

class Roles
{
    private $id;
    
    /**
     * @Assert\Length(max = 50, maxMessage = "Le nom du role ne peut pas dépasser {{ limit }} caractères")
     * @Assert\NotBlank(message = "Le nom du role doit être présent")
     */
    private $nom;
    
    /**
     */
    private $actif;
    
    /**
     */
    private $personne_roles;

    public function getId(){ return $this->id; }

    public function setNom($x){ $this->nom = $x; return $this; }
    public function getNom(){ return $this->nom; }
    
    public function setActif($x){ $this->actif = $x; return $this; }
    public function getActif(){ return $this->actif; }
    
    public function setPersonneRoles($x){ $this->personne_roles = $x; return $this; }
    public function getPersonneRoles(){ return $this->personne_roles; }
}

