<?php

namespace AppBundle\Entity;

class PersonneRoles
{
    private $id;
    private $personne;
    private $role;

    public function getId(){ return $this->id; }

    public function setPersonne($x){ $this->personne = $x; return $this; }
    public function getPersonne(){ return $this->personne; }
    
    public function setRole($x){ $this->role = $x; return $this; }
    public function getRole(){ return $this->role; }
}

