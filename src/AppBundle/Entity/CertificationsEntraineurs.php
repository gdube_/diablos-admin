<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CertificationsEntraineurs
 *
 * @ORM\Table(name="certifications_entraineurs", indexes={@ORM\Index(name="id_entraineur", columns={"id_entraineur"})})
 * @ORM\Entity
 */
class CertificationsEntraineurs
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_certification", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="annee_obtention", type="integer", nullable=false)
     */
    private $anneeObtention;

    /**
     * @var string
     *
     * @ORM\Column(name="titre", type="string", length=50, nullable=false)
     */
    private $titre;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255, nullable=false)
     */
    private $description;

    /**
     * @var \AppBundle\Entity\Entraineurs
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Entraineurs")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_entraineur", referencedColumnName="id_entraineur")
     * })
     */
    private $idEntraineur;


}

