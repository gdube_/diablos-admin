<?php

namespace AppBundle\Entity;

class ActivitePersonneRoles
{
    private $id;
    private $personne;
    private $role;
    private $activite;

    public function getId(){ return $this->id; }

    public function setPersonne($x){ $this->personne = $x; return $this; }
    public function getPersonne(){ return $this->personne; }
    
    public function setRole($x){ $this->role = $x; return $this; }
    public function getRole(){ return $this->role; }
    
    public function setActivite($x){ $this->activite = $x; return $this; }
    public function getActivite(){ return $this->activite; }
}

