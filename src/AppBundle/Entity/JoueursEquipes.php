<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * JoueursEquipes
 *
 * @ORM\Table(name="joueurs_equipes", indexes={@ORM\Index(name="id_joueur", columns={"id_joueur"}), @ORM\Index(name="id_equipe", columns={"id_equipe"}), @ORM\Index(name="id_position", columns={"id_position"})})
 * @ORM\Entity
 */
class JoueursEquipes
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_joueur_equipe", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="numero", type="integer", nullable=false)
     */
    private $numero;

    /**
     * @var string
     *
     * @ORM\Column(name="photo_profil", type="string", length=255, nullable=false)
     */
    private $photoProfil;

    /**
     * @var \AppBundle\Entity\Positions
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Positions")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_position", referencedColumnName="id_position")
     * })
     */
    private $idPosition;

    /**
     * @var \AppBundle\Entity\Equipes
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Equipes")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_equipe", referencedColumnName="id_equipe")
     * })
     */
    private $idEquipe;

    /**
     * @var \AppBundle\Entity\Joueurs
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Joueurs")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_joueur", referencedColumnName="id_joueur")
     * })
     */
    private $idJoueur;


}

