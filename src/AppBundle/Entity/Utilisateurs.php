<?php

namespace AppBundle\Entity;

use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;

class Utilisateurs implements UserInterface, \Serializable
{
    private $id;
    
    /**
     * @Assert\Length(max = 30, maxMessage = "Le nom d'utilisateur ne peut pas dépasser {{ limit }} caractères")
     * @Assert\NotBlank(message = "Le nom d'utilisateur doit être présent")
     */
    private $nom_utilisateur;
    
    /**
     * @Assert\Length(max = 30, maxMessage = "Le mot de passe ne peut pas dépasser {{ limit }} caractères")
     * @Assert\NotBlank(message = "Le mot de passe doit être présent")
     */
    private $mot_de_passe;
    
    /**
     * @Assert\NotBlank(message = "Le niveau d'accès doit être présent")
     */
    private $niveau_acces;
    
    
    public function getRoles(){ return array($this->niveau_acces); }
    public function getPassword(){ return $this->mot_de_passe; }
    public function getUsername(){ return $this->nom_utilisateur; }
    public function eraseCredentials(){ }
    public function getSalt(){return null;}

    public function getId(){ return $this->id; }

    public function setNomUtilisateur($x){ $this->nom_utilisateur = $x; return $this; }
    public function getNomUtilisateur(){ return $this->nom_utilisateur; }
    
    public function setMotDePasse($x){ $this->mot_de_passe = $x; return $this; }
    public function getMotDePasse(){ return $this->mot_de_passe; }
    
    public function setNiveauAcces($x){ $this->niveau_acces = $x; return $this; }
    public function getNiveauAcces(){ return $this->niveau_acces; }
    
    /** @see \Serializable::serialize() */
    public function serialize()
    {
        return serialize(array(
            $this->id,
            $this->nom_utilisateur,
            $this->mot_de_passe,
        ));
    }

    /** @see \Serializable::unserialize() */
    public function unserialize($serialized)
    {
        list (
            $this->id,
            $this->nom_utilisateur,
            $this->mot_de_passe,
        ) = unserialize($serialized);
    }

}

