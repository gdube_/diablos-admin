<?php

namespace AppBundle\Entity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * ReservationsHotels
 */
class ReservationsHotels
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     * @Assert\Length(max = 50, maxMessage = "Le nom de l'hotel ne peut pas dépasser {{ limit }} caractères")
     */
    private $hotel;

    /**
     * @var \DateTime
     * @Assert\DateTime(message = "L'heure de départ n'est pas valide")
     */
    private $heureDepart;

    /**
     * @var \DateTime
     * @Assert\DateTime(message = "L'heure de retour n'est pas valide")
     */
    private $heureRetour;

    /**
     * @var \DateTime
     * @Assert\DateTime(message = "L'heure de retour n'est pas valide")
     */
    private $dateReservation;

    /**
     * @var string
     */
    private $contact;

    /**
     * @var string
     * @Assert\Length(max = 20, maxMessage = "La demande d'achat ne peut pas dépasser {{ limit }} caractères")
     */
    private $demandeAchat;

    /**
     * @var string
     * @Assert\Length(max = 100, maxMessage = "L'adresse ne peut pas dépasser {{ limit }} caractères")
     */
    private $adresse;

    /**
     * @var string
     * @Assert\Length(max = 255, maxMessage = "La note ne peut pas dépasser {{ limit }} caractères")
     */
    private $note;

    /**
     * @var integer
     * @Assert\GreaterThan(value = 0, message = "Il doit y avoir au moins une chambre de réservée.")
     * @Assert\LessThan(value = 100, message = "Il ne peut y avoir plus de 99 réservées.")
     */
    private $nombreChambre;

    /**
     * @var integer
     * @Assert\GreaterThan(value = 0, message = "La réservation doit être pour au moins une nuit.")
     * @Assert\LessThan(value = 90, message = "La réservation ne peut pas se prolongé pour plus de 3 mois (90 jours).")
     */
    private $nombreNuit;
    
    /**
     * @var \AppBundle\Entity\Activites
     */
    private $activite;
    
    function get_date($field, $formatted = true, $format = 'Y/m/d H:i') {
      if (isset($field) && !empty($field)){ 
        if($formatted){
          return $field->format($format);
        }else{
          return $field;
        }
      }else{
        return null;
      }
    }
    
    
    function getId() {return $this->id;}
    function getHotel() {return $this->hotel;}
    function getContact() { 
      if(isset($this->contact))
        return $this->contact; 
      else
        return "{}";
    }
    function getDemandeAchat() {return $this->demandeAchat;}
    function getAdresse() {return $this->adresse;}
    function getNote() {return $this->note;}
    function getNombreChambre() {return $this->nombreChambre;}
    function getNombreNuit() {return $this->nombreNuit;}
    function getActivite() {return $this->activite;}
    
    function getHeureDepart($formatted = true, $format = 'Y/m/d'){
      return $this->get_date($this->heureDepart, $formatted, $format);
    }
    
    function getHeureRetour($formatted = true, $format = 'Y/m/d'){
      return $this->get_date($this->heureRetour, $formatted, $format);
    }
    
    function getDateReservation($formatted = true, $format = 'Y/m/d'){
      return $this->get_date($this->dateReservation, $formatted, $format);
    }
    
    
    function setHotel($x) {$this->hotel = $x; return $this;}
    function setContact($x) {$this->contact = $x; return $this;}
    function setDemandeAchat($x) {$this->demandeAchat = $x; return $this;}
    function setAdresse($x) {$this->adresse = $x; return $this;}
    function setNote($x) {$this->note = $x; return $this;}
    function setNombreChambre($x) {$this->nombreChambre = $x; return $this;}
    function setNombreNuit($x) {$this->nombreNuit = $x; return $this;}
    function setActivite($x) {$this->activite = $x; return $this;}
    
    public function setHeureDepart($x) {
      if(empty($x) || $x === null){
        $this->heureDepart = null;
      }else{
        $this->heureDepart = \DateTime::createFromFormat('Y/m/d', $x);
      }
      
      return $this;
    }
    
    public function setHeureRetour($x) {
      if(empty($x) || $x === null){
        $this->heureDepart = null;
      }else{
        $this->heureRetour = \DateTime::createFromFormat('Y/m/d', $x);
      }
      
      return $this;
    }
    
    public function setDateReservation($x) {
      if(empty($x) || $x === null){
        $this->dateReservation = null;
      }else{
        $this->dateReservation = \DateTime::createFromFormat('Y/m/d', $x);
      }
      
      return $this;
    }
    
}
