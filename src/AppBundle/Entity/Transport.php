<?php

namespace AppBundle\Entity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Transport
 */
class Transport
{
    /**
     * @var integer
     */
    private $id;
  
    /**
     * @var \DateTime
     * @Assert\DateTime(message = "L'heure de départ n'est pas valide")
     */
    private $heureDepart;

    /**
     * @var \DateTime
     * @Assert\DateTime(message = "L'heure de retour n'est pas valide")
     */
    private $heureRetour;

    /**
     * @var string
     * @Assert\Length(max = 15, maxMessage = "La demande d'achat ne peut pas dépasser {{ limit }} caractères")
     */
    private $demandeAchat;

    /**
     * @var \DateTime
     * @Assert\DateTime(message = "La date ne réservation n'est pas valide")
     */
    private $dateReservation;

    /**
     * @var string
     * @Assert\Length(max = 50, maxMessage = "Le nom de la compagnie ne peut pas dépasser {{ limit }} caractères")
     */
    private $compagnie;

    /**
     * @var string
     * @Assert\Length(max = 25, maxMessage = "Le type de transport ne peut pas dépasser {{ limit }} caractères")
     */
    private $typeTransport;

    /**
     * @var string
     * @Assert\Length(max = 255, maxMessage = "La note ne peut pas dépasser {{ limit }} caractères")
     */
    private $note;
    
    /**
     * @var string
     */
    private $contact;

    /**
     * @var \AppBundle\Entity\Activites
     */
    private $activite;
    
    function get_date($field, $formatted = true, $format = 'Y/m/d H:i') {
      if (isset($field) && !empty($field)){ 
        if($formatted){
          return $field->format($format);
        }else{
          return $field;
        }
      }else{
        return null;
      }
    }

    function getId() {return $this->id;}
    function getDemandeAchat() {return $this->demandeAchat;}
    function getCompagnie() {return $this->compagnie;}
    function getTypeTransport() {return $this->typeTransport;}
    function getNote() {return $this->note;}
    function getContact() { 
      if(isset($this->contact))
        return $this->contact; 
      else
        return "{}";
    }
    function getActivite() {return $this->activite;}
    
    function getHeureDepart($formatted = true, $format = 'Y/m/d'){
      return $this->get_date($this->heureDepart, $formatted, $format);
    }
    
    function getHeureRetour($formatted = true, $format = 'Y/m/d'){
      return $this->get_date($this->heureRetour, $formatted, $format);
    }
    
    function getDateReservation($formatted = true, $format = 'Y/m/d'){
      return $this->get_date($this->dateReservation, $formatted, $format);
    }
    
    function setDemandeAchat($x) {$this->demandeAchat = $x; return $this;}
    function setCompagnie($x) {$this->compagnie = $x; return $this;}
    function setTypeTransport($x) {$this->typeTransport = $x; return $this;}
    function setNote($x) {$this->note = $x; return $this;}
    function setContact($x) {$this->contact = $x; return $this;}
    function setActivite($x) {$this->activite = $x; return $this;}
    
    public function setHeureDepart($x) {
      if(empty($x) || $x === null){
        $this->heureDepart = null;
      }else{
        $this->heureDepart = \DateTime::createFromFormat('Y/m/d', $x);
      }
      
      return $this;
    }
    
    public function setHeureRetour($x) {
      if(empty($x) || $x === null){
        $this->heureDepart = null;
      }else{
        $this->heureRetour = \DateTime::createFromFormat('Y/m/d', $x);
      }
      
      return $this;
    }
    
    public function setDateReservation($x) {
      if(empty($x) || $x === null){
        $this->heureDepart = null;
      }else{
        $this->dateReservation = \DateTime::createFromFormat('Y/m/d', $x);
      }
      return $this;
    }
   
}
