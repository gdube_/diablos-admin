<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Equipes
 *
 * @ORM\Table(name="equipes", indexes={@ORM\Index(name="id_sport", columns={"id_sport"})})
 * @ORM\Entity
 */
class Equipes
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_equipe", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=50, nullable=false)
     * @Assert\Length(max = 50, maxMessage = "Le nom de l'équipe ne peut pas dépasser {{ limit }} caractères")
     * @Assert\NotBlank(message = "Le nom de l'équipe doit être présent")
     */
    private $nom;

    /**
     * @var string
     *
     * @ORM\Column(name="niveau", type="string", length=50, nullable=false)
     * @Assert\Length(max = 50, maxMessage = "Le niveau de l'équipe ne peut pas dépasser {{ limit }} caractères")
     */
    private $niveau;

    /**
     * @var string
     *
     * @ORM\Column(name="sexe", type="string", length=1, nullable=false)
     */
    private $sexe;

    /**
     * @var string
     *
     * @ORM\Column(name="saison", type="string", length=20, nullable=false)
     * @Assert\Length(max = 20, maxMessage = "La saison de l'équipe ne peut pas dépasser {{ limit }} caractères")
     */
    private $saison;

    /**
     * @var string
     *
     * @ORM\Column(name="photo_equipe", type="string", length=255)
     * @Assert\Length(max = 255, maxMessage = "Le nom de l'image de l'équipe ne peut pas dépasser {{ limit }} caractères")
     */
    private $photoEquipe;
    
    /**
     * @var boolean
     *
     * @ORM\Column(name="actif", type="boolean", nullable=false)
     */
    private $actif;

    /**
     * @var \AppBundle\Entity\Sports
     *
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Sports")
     * @ORM\JoinColumns({@ORM\JoinColumn(name="id_sport", referencedColumnName="id_sport")})
     * @Assert\Valid()
     */
    private $sport;



    function getId() {return $this->id;}
    function getNom() {return $this->nom;}
    function getNiveau() {return $this->niveau;}
    function getSexe() {return $this->sexe;}
    function getSaison() {return $this->saison;}
    function getPhotoEquipe() {return $this->photoEquipe;}
    function getActif() {return $this->actif;}
    function getSport() {return $this->sport;}
    
    function getNomComplet() {return $this->nom.' '.$this->sexe." ".$this->niveau;}
    function getNomAbrev() {return $this->nom.' '.$this->sexe;}
    
    function setId($id) {$this->id = $id; return $this;}
    function setNom($nom) {$this->nom = $nom; return $this;}
    function setNiveau($niveau) {$this->niveau = $niveau; return $this;}
    function setSexe($sexe) {$this->sexe = $sexe; return $this;}
    function setSaison($saison) {$this->saison = $saison; return $this;}
    function setPhotoEquipe($photoEquipe) {$this->photoEquipe = $photoEquipe; return $this;}
    function setActif($actif) {$this->actif = $actif; return $this;}
    function setSport($sport) {$this->sport = $sport; return $this;}

}

