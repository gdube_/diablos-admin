<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Activites
 *
 * @ORM\Table(name="activites")
 * @ORM\Entity
 */
class Activites
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var \DateTime
     *
     */
    private $dateHeure;

    /**
     * @var string
     *
     * @Assert\Length(max = 50, maxMessage = "Le nom du visiteur ne peut pas dépasser {{ limit }} caractères")
     * @Assert\NotBlank(message = "Le nom du visiteur doit être présent")
     */
    private $visiteur;

    /**
     * @var string
     *
     * @Assert\Length(max = 50, maxMessage = "Le nom du receveur ne peut pas dépasser {{ limit }} caractères")
     * @Assert\NotBlank(message = "Le nom du receveur doit être présent")
     */
    private $receveur;

    /**
     * @var string
     *
     * @Assert\Length(max = 100, maxMessage = "Le nom du lieu ne peut pas dépasser {{ limit }} caractères")
     * @Assert\NotBlank(message = "Le nom du lieu doit être présent")
     */
    private $lieu;

    /**
     * @var string
     *
     * @Assert\Length(max = 50, maxMessage = "Le nom du type d'activité ne peut pas dépasser {{ limit }} caractères")
     * @Assert\NotBlank(message = "Le nom du type d'activité doit être présent")
     */
    private $type;
    
    /**
     * @var string
     *
     * @Assert\NotBlank(message = "Le statut doit être présent")
     */
    private $statut;

    /**
     * @var \AppBundle\Entity\Equipes
     * @Assert\Valid()
     */
    private $equipe;
    
     /**
     * @var \AppBundle\Entity\Transport
     * @Assert\Valid()
     */
    private $transport;
    
    /**
     * @var \AppBundle\Entity\ReservationsHotels
     * @Assert\Valid()
     */
    private $reservationHotel;
    
    
    function get_date($field, $formatted = true, $format = 'Y/m/d H:i') {
      if (isset($field)){ 
        if($formatted){
          return $field->format($format);
        }else{
          return $field;
        }
      }else{
        return null;
      }
    }    

    function getId() {return $this->id;}
    function getType() {return $this->type;}
    function getLieu() {return $this->lieu;}
    function getReceveur() {return $this->receveur;}
    function getVisiteur() {return $this->visiteur;}
    function getEquipe() {return $this->equipe;}
    function getTransport() {return $this->transport;}
    function getReservationHotel() {return $this->reservationHotel;}
    function getStatut() {
      if(isset($this->statut))
        return $this->statut; 
      else
        return "{\"status\": \"green\"}";
    }

    public function getDateHeure($formatted = true, $format = 'Y/m/d H:i') { 
      return $this->get_date($this->dateHeure, $formatted, $format);
    }
    
    public function getDateHeureTraduit(){
      $format = 'd MMMM HH:mm';
      if($this->dateHeure->format('H:i') == '00:00')
        $format = 'd MMMM';
        
      $fmt = new \IntlDateFormatter(
        'fr-CA',
        \IntlDateFormatter::FULL,
        \IntlDateFormatter::FULL,
        'Europe/Paris',
        \IntlDateFormatter::GREGORIAN,
        $format
        );
      return $fmt->format($this->dateHeure);
    }

    public function getNom(){
      return $this->type." ".$this->equipe->getNomComplet();
    }

    public function setDateHeure($dateHeure) {
      $this->dateHeure = \DateTime::createFromFormat('Y/m/d H:i', $dateHeure); return $this;
    }
    function setLieu($lieu)    {$this->lieu = $lieu; return $this;}
    function setType($type)    {$this->type = $type; return $this;}
    function setReceveur($receveur)  {$this->receveur = $receveur; return $this;}
    function setVisiteur($visiteur)  {$this->visiteur = $visiteur; return $this;}
    function setEquipe($equipe)  {$this->equipe = $equipe; return $this;}
    function setStatut($statut) {$this->statut = $statut; return $this;}
    function setTransport($transport)  {$this->transport = $transport; return $this;}
    function setReservationHotel($res)  {$this->reservationHotel = $res; return $this;}
    
}

