<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Personnes
 *
 * @ORM\Table(name="personnes")
 * @ORM\Entity
 */
class Personnes
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     *
     * @Assert\NotBlank(message = "Le nom de la personne doit être présent")
     * @Assert\Length(max = 50, maxMessage = "Le nom ne peut pas dépasser {{ limit }} caractères")
     */
    private $nom;

    /**
     * @var string
     *
     * @Assert\NotBlank(message = "Le prenom de la personne doit être présent")
     * @Assert\Length(max = 50, maxMessage = "Le prenom ne peut pas dépasser {{ limit }} caractères")
     */
    private $prenom;

    /**
     * @var string
     */
    private $sexe;

    /**
     * @var \DateTime
     *
     * \\@Assert\Date(message="La date de naissance n'est pas valide")
     */
    private $dateNaissance;

    /**
     * @var string
     *
     * @Assert\Length(max = 150, maxMessage = "L'adresse courriel ne peut pas dépasser {{ limit }} caractères")
     * @Assert\Email(message = "L'adresse courriel n'est pas valide.", strict = false)
     */
    private $courriel;
    
    /**
     * @var string
     *
     * @Assert\Length(max = 30, maxMessage = "La rue ne peut pas dépasser {{ limit }} caractères")
     */
    private $rue;
    
    /**
     * @var string
     *
     * @Assert\Length(max = 30, maxMessage = "La ville ne peut pas dépasser {{ limit }} caractères")
     */
    private $ville;
    
    /**
     * @var string
     *
     * @Assert\Length(max = 25, maxMessage = "La province ne peut pas dépasser {{ limit }} caractères")
     */
    private $province;
    
    /**
     * @var string
     *
     * @Assert\Regex(pattern="/[A-Z]\d[A-Z] \d[A-Z]\d/", message="Le code postal doit avoir le format suivant: XXX XXX")
     */
    private $code_postal;
    

    /**
     * @var string
     *
     */
    private $noTel;

    /**
     * @var string
     *
     * @Assert\Length(max = 25, maxMessage = "La note ne peut pas dépasser {{ limit }} caractères")
     */
    private $note;
    

    /**
     * @var boolean
     */
    private $actif;
    
    /**
     */
    private $joueur;
    
    /**
    */
    private $auth_embauche;
    
    /**
    */
    private $personne_roles;


    public function getId() {return $this->id; }

    public function getNom() { return $this->nom; }
    public function setNom($x) { $this->nom = $x; return $this; }
    
    public function getPrenom() { return $this->prenom; }
    public function setPrenom($x) { $this->prenom = $x; return $this; }
    
    public function getSexe() { return $this->sexe; }
    public function setSexe($x) { $this->sexe = $x; return $this; }
    
    public function getDateNaissance() { 
      if (isset($this->dateNaissance)){
        return $this->dateNaissance->format('Y/m/d');
      }else{
        return null;
      }
    }
    public function setDateNaissance($x) {
      if(isset($x))
        $this->dateNaissance = \DateTime::createFromFormat('Y/m/d', $x);
      else
        $this->dateNaissance = null;
      return $this;
    }
    
    public function getVille() { return $this->ville; }
    public function setVille($x) { $this->ville = $x; return $this; }
    
    public function getRue() { return $this->rue; }
    public function setRue($x) { $this->rue = $x; return $this; }
    
    public function getProvince() { return $this->province; }
    public function setProvince($x) { $this->province = $x; return $this; }
    
    public function getCodePostal() { return $this->code_postal; }
    public function setCodePostal($x) { $this->code_postal = strtoupper($x); return $this; }
    
    public function getAddress() { return $this->rue . ", " . $this->ville . ", " . $this->province; }
    
    public function getCourriel() { return $this->courriel; }
    public function setCourriel($x) { $this->courriel = $x; return $this; }
    
    public function getNoTel() { 
      if(isset($this->noTel))
        return $this->noTel; 
      else
        return "{}";
    }
    public function setNoTel($x) { $this->noTel = $x; return $this; }
    
    public function getNote() { return $this->note; }
    public function setNote($x) { $this->note = $x; return $this; }
    
    public function getActif() { return $this->actif; }
    public function setActif($x) { $this->actif = $x; return $this; }
    
    public function getAuthEmbauche() { 
      if(isset($this->auth_embauche))
        return $this->auth_embauche; 
      else
        return "{}";
    }
    public function setAuthEmbauche($x) { $this->auth_embauche = $x; return $this; }
    
    public function getPersonneRoles() { return $this->personne_roles; }
    public function setPersonneRoles($x) { $this->personne_roles = $x; return $this; }
    
    public function getNomComplet($inv=true) {
      if($inv)
        return $this->nom . ', ' . $this->prenom;
      else
        return $this->prenom . ' ' . $this->nom;
    }
    
    public function __construct(){
      $this->setActif(true);
    }
}

