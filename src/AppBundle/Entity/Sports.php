<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Sports
 *
 * @ORM\Table(name="sports")
 * @ORM\Entity
 */
class Sports
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_sport", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="sport", type="string", length=50, nullable=false)
     * @Assert\Length(max = 50, maxMessage = "Le nom du sport ne peut pas dépasser {{ limit }} caractères")
     * @Assert\NotBlank(message = "Le nom du sport doit être présent")
     */
    private $sport;

    public function getId() { return $this->id; }
    
    public function getSport() { return $this->sport; }
    public function setSport($x) { $this->sport = $x; return $this;}

}

