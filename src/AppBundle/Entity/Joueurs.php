<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

use AppBundle\Entity\Personnes;

/**
 * Joueurs
 *
 * @ORM\Table(name="joueurs", indexes={@ORM\Index(name="id_personne", columns={"id_personne"})})
 * @ORM\Entity
 */
class Joueurs
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_joueur", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="taille", type="integer", nullable=false)
     * @Assert\NotBlank(message = "La taille du joueur doit être doit être présente")
     * @Assert\GreaterThan(value = 70, message = "La grandeur du joueur doit être supérieur à 70cm ( 28 pouces ) ")
     * @Assert\LessThan(value = 272, message = "La grandeur du joueur doit être inférieur à 272cm ( 107 pouces ) ")
     */
    private $taille;

    /**
     * @var integer
     *
     * @ORM\Column(name="taille", type="integer", nullable=false, options={"unsigned"=true})
     * @Assert\NotBlank(message = "Le poids du joueur doit être présent")
     * @Assert\GreaterThan(value = 31, message = "Le poids du joueur doit être supérieur à 31kg ( 68 lb ) ")
     * @Assert\LessThan(value = 200, message = "Le poids du joueur doit être inférieur à 200kg ( 440 lb ) ")
     */
    private $poids;

    /**
     * @var string
     *
     * @ORM\Column(name="note", type="string", length=255)
     * @Assert\Length(max = 255, maxMessage = "La note ne peut pas dépasser {{ limit }} caractères")
     */
    private $note;

    /**
     * @var \AppBundle\Entity\Personnes
     *
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Personnes")
     * @ORM\JoinColumns({@ORM\JoinColumn(name="id_personne", referencedColumnName="id_personne")})
     * @Assert\Valid()
     */
    private $personne;
    
    public function __construct(){
      $this->setPersonne(new Personnes());
    }
    
    public function getId() { return $this->id; }
    
    public function getTaille() { return $this->taille; }
    public function setTaille($x) { $this->taille = $x; return $this;}
    
    public function getPoids() { return $this->poids; }
    public function setPoids($x) { $this->poids = $x; return $this;}
    
    public function getNote() { return $this->note; }
    public function setNote($x) { $this->note = $x; return $this;}
    
    public function getPersonne() { return $this->personne; }
    public function setPersonne($x) { $this->personne = $x; return $this;}

    public function getRole() { return null; }
}

