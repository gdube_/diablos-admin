<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * EntraineurEquipe
 *
 * @ORM\Table(name="entraineur_equipe", indexes={@ORM\Index(name="id_entraneur", columns={"id_entraineur"}), @ORM\Index(name="id_equipe", columns={"id_equipe"})})
 * @ORM\Entity
 */
class EntraineurEquipe
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_entraineur_equipe", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="role", type="string", length=30, nullable=false)
     */
    private $role;

    /**
     * @var string
     *
     * @ORM\Column(name="photo_profil", type="string", length=255, nullable=false)
     */
    private $photoProfil;

    /**
     * @var \AppBundle\Entity\Equipes
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Equipes")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_equipe", referencedColumnName="id_equipe")
     * })
     */
    private $idEquipe;

    /**
     * @var \AppBundle\Entity\Entraineurs
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Entraineurs")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_entraineur", referencedColumnName="id_entraineur")
     * })
     */
    private $idEntraineur;


}

