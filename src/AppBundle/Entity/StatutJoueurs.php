<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * StatutJoueurs
 *
 * @ORM\Table(name="statut_joueurs", indexes={@ORM\Index(name="id_joueur_equipe", columns={"id_joueur_equipe"})})
 * @ORM\Entity
 */
class StatutJoueurs
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_statut", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="statut", type="string", length=200, nullable=false)
     */
    private $statut;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_arret", type="date", nullable=false)
     */
    private $dateArret;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_retour", type="date", nullable=false)
     */
    private $dateRetour;

    /**
     * @var \AppBundle\Entity\JoueursEquipes
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\JoueursEquipes")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_joueur_equipe", referencedColumnName="id_joueur_equipe")
     * })
     */
    private $idJoueurEquipe;


}

