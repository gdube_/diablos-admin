<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Sports;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

use AppBundle\Entity\Equipes;

class EquipesController extends Controller
{
    /**
     * @Route("/equipes", name="equipes-index")
     */
    public function indexAction(Request $request)
    {
        $count = $this->getDoctrine()->getManager()->createQuery('SELECT COUNT(p.id) FROM AppBundle:Equipes p')->getSingleScalarResult();
        $limit = 20;
        $page = 0;
        $max_page = ceil($count/$limit);
        $vars = array("limit" => $limit, "page" => $page, "max_page"=>$max_page);
        
        $errors = $request->query->get("extra_errors");
        if(isset($errors)) $vars["extra_errors"] = $errors; 
        
        $flash = $request->query->get("flash");
        if(isset($flash)) $vars["flash"] = $flash; 
        
        $vars["user"] = $this->getUser();
        
        return $this->render('equipes/index.html.twig', $vars);
    }
    
    /**
     * @Route("/equipes/new", name="equipe-new")
     */
    public function newAction(Request $request)
    {
        $equipe = new Equipes();
        $equipe->setActif(true);
        $form = $this->equipesForm($equipe, "Nouvelle Equipe");
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
         
          $em = $this->getDoctrine()->getManager();
          
          $equipe = $form->getData();
          $sport = new Sports();
          $sport->setSport($equipe->getNom());
          
          $em->persist($sport);
          $em->persist($equipe->setSport($sport));
          $em->flush();
          
          return $this->redirectToRoute('equipes-index', array("flash" => "L'équipe a été créée"));
        }
            
        $vars = array('form' => $form->createView(), 'content_title' => "Nouvelle Équipe", 'user' => $this->getUser());    
        
        return $this->render('equipes/form.html.twig', $vars);
    }
    
    /**
     * @Route("/equipes/{id}", name="equipe-edit")
     */
    public function editAction(Request $request, $id)
    {
      $em = $this->getDoctrine()->getManager();
      $query = $em->createQuery(
        'SELECT e FROM AppBundle:Equipes e WHERE e.id = :id'
      )->setParameter('id', $id);
      
      $equipe = $query->setMaxResults(1)->getOneOrNullResult();
      if (!$equipe) {
          $errors = array("L'équipe n'existe pas.");
          return $this->redirectToRoute('equipes-index', array("extra_errors" => $errors));
      }
      
      $form = $this->equipesForm($equipe, "Modifier l'équipe");
      $form->handleRequest($request);
      if ($form->isSubmitted() && $form->isValid()) {
        $em = $this->getDoctrine()->getManager();
        $equipe = $form->getData();
        $em->flush();
        return $this->redirectToRoute('equipes-index', array("flash" => "L'équipe '".$equipe->getNomComplet()."' a été modifiée"));
      }
      
      $vars = array('form' => $form->createView(), 'content_title' => "Modifier une équipe", 'user' => $this->getUser());    
      return $this->render('equipes/form.html.twig', $vars);
    }
    
    /**
    * @Route("/equipes/{id}/delete.json", name="equipes-delete")
    */
    public function deleteAction(Request $request, $id)
    {
      $em = $this->getDoctrine()->getManager();
      $query = $em->createQuery(
        'SELECT e FROM AppBundle:Equipes e JOIN e.sport s WHERE e.id = :id'
      )->setParameter('id', $id);

      $equipe = $query->setMaxResults(1)->getOneOrNullResult();
      if(!$equipe){
        $res = new JsonResponse(array('error' => "L'équipe n'existe pas"));
        $res->setStatusCode(404, null);
        return $res;
      }

      $equipe->setActif(false);
      $em->flush();
      
      $msg = "L'équipe '". $equipe->getNomComplet() ."' a été désactivée";
      return new JsonResponse(array('success' => $msg ));
    }
    
    /**
     * @Route("/equipes-recherche.json", name="liste-equipes")
     */
     public function listeEquipes(Request $request)
     {
         $page = $request->query->get('page');
         $limit = $request->query->get('limit');
         $data = array("page" => $page, "equipes" => array());
         
         $em = $this->getDoctrine()->getManager();
         $query = $em->createQuery('
           SELECT e, s FROM AppBundle:Equipes e JOIN e.sport s
           WHERE e.actif = TRUE
           ORDER BY s.sport
         ');
         $query->setMaxResults($limit);
         $query->setFirstResult($limit*$page);
        
         foreach ($query->getResult() as $e){
            $data["equipes"][] = array(
              "nom" => $e->getNomComplet(),
              "id" => $e->getId()
              );
         }
         
         return new JsonResponse($data);
     }
     
    
    private function equipesForm($equipe, $submit_label){

      $sexe_list = array('Masculin ' => 'Masculin', 'Féminin' => 'Féminin', 'Mixte' => 'Mixte');
      $form = $this->createFormBuilder($equipe)
          ->add('nom', TextType::class, array('label' => 'Nom', 'error_bubbling' => true))
          ->add('niveau', TextType::class, array('label' => 'Niveau', 'error_bubbling' => true, 'required' => false))
          ->add('sexe', ChoiceType::class,
            array( 'choices'  => $sexe_list, 'choices_as_values' => true, 'expanded' => true,
            'multiple' => false, 'label' => "Sexe"))
          ->add('save', SubmitType::class, array('label' => $submit_label))
          ->getForm();
      return $form;
    }
    
}

