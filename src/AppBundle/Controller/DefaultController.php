<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        $vars = array();
        
        $errors = $request->query->get("extra_errors");
        if(isset($errors)) $vars["extra_errors"] = $errors; 
        
        $flash = $request->query->get("flash");
        if(isset($flash)) $vars["flash"] = $flash; 
        
        $vars["user"] = $this->getUser();
        
        return $this->render('default/index.html.twig', $vars);
    }
    
    /**
     * @Route("/login", name="login")
     */
    public function loginAction(Request $request)
    {
        $vars = array();
        
        $errors = $request->query->get("extra_errors");
        if(isset($errors)) $vars["extra_errors"] = $errors; 
        
        $flash = $request->query->get("flash");
        if(isset($flash)) $vars["flash"] = $flash; 
        
        $vars["user"] = $this->getUser();
        if($vars["user"] !== null){
          $errors = array("Déjà connecté.");
          return $this->redirectToRoute('homepage', array("extra_errors" => $errors));
        }
        
        return $this->render('default/login.html.twig', $vars);
    }
    
    /**
     * @Route("/no_access", name="no_access")
     */
    public function noAccessAction(Request $request)
    {
        $vars = array("user"=> $this->getUser());
        return $this->render('default/no_access.html.twig', $vars);
    }
}
