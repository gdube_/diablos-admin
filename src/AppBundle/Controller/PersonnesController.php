<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

use AppBundle\Form\Type\PersonneType;
use AppBundle\Entity\Personnes;
use AppBundle\Entity\PersonneRoles;

class PersonnesController extends Controller
{

    /**
     * @Route("/personnes", name="personnes-index")
     */
    public function indexAction(Request $request)
    {
        $count = $this->getDoctrine()->getManager()->createQuery('SELECT COUNT(p.id) FROM AppBundle:Personnes p')->getSingleScalarResult();
        $limit = 20;
        $page = 0;
        $max_page = round($count/$limit, 0, PHP_ROUND_HALF_UP);
        $vars = array("limit" => $limit, "page" => $page, "max_page"=>$max_page);
        
        $errors = $request->query->get("extra_errors");
        if(isset($errors)) $vars["extra_errors"] = $errors; 
        
        $flash = $request->query->get("flash");
        if(isset($flash)) $vars["flash"] = $flash; 
        
        $vars["user"] = $this->getUser();
        
        return $this->render('personnes/index.html.twig', $vars);
    }
    
    /**
     * @Route("/personnes/new", name="personnes-new")
     */
    public function newAction(Request $request)
    {
        $personne = new Personnes();
        $form = $this->personnesForm($personne, "Nouvelle Personne");
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
          $roles_id = $request->request->get('roles');
          $personne = $form->getData();
          $nom = $personne->getNomComplet(false);
          
          $em = $this->getDoctrine()->getManager();
          $em->persist($personne);
          $em->flush();
          $this->bindRoles($personne, $roles_id);
          return $this->redirectToRoute('personnes-index', array("flash" => $nom." a été créé"));
        }
        
        $vars = array('form' => $form->createView(), 'content_title' => "Nouvelle Personne", 'user' => $this->getUser());    
        $this->listRoles($vars, $personne);  
        return $this->render('personnes/form.html.twig', $vars);
    }
    
    /**
     * @Route("/personnes/{id}", name="personnes-edit")
     */
    public function editAction(Request $request, $id)
    {
      $em = $this->getDoctrine()->getManager();
      $query = $em->createQuery(
        'SELECT p FROM AppBundle:Personnes p WHERE p.id = :id'
      )->setParameter('id', $id);
      
      $personne = $query->setMaxResults(1)->getOneOrNullResult();
      if (!$personne) {
          $errors = array("La personne n'existe pas.");
          return $this->redirectToRoute('personnes-index', array("extra_errors" => $errors));
      }
      
      $form = $this->personnesForm($personne, "Modifier la personne");
      $form->handleRequest($request);
      if ($form->isSubmitted() && $form->isValid()) {
        $roles_id = $request->request->get('roles');
        
        $em = $this->getDoctrine()->getManager();
        $personne = $form->getData();
        $em->flush();
        $this->bindRoles($personne, $roles_id);
        $nom = $personne->getNomComplet();
        return $this->redirectToRoute('personnes-index', array("flash" => $nom." a été modifié"));
      }
      
      $vars = array('form' => $form->createView(), 'content_title' => "Modifier une personne", 'user' => $this->getUser());  
      $this->listRoles($vars, $personne);  
      return $this->render('personnes/form.html.twig', $vars);
    }
    
    /**
    * @Route("/personnes/{id}/delete.json", name="personnes-delete")
    */
    public function deleteAction(Request $request, $id)
    {
      $em = $this->getDoctrine()->getManager();
      $query = $em->createQuery(
        'SELECT p FROM AppBundle:Personnes p WHERE p.id = :id'
      )->setParameter('id', $id);
      
      $personne = $query->setMaxResults(1)->getOneOrNullResult();
      if (!$personne) {
        $res = new JsonResponse(array('error' => "Cette personne n'existe pas"));
        $res->setStatusCode(404, null);
        return $res;
      }
      
      $personne->setActif(false);
      $em->flush();
      
      $msg = $personne->getNomComplet(false)." a été désactivée";
      return new JsonResponse(array('success' => $msg ));
    }
    
    /**
     * @Route("/personnes-recherche.json", name="recherche-personnes")
     */
     public function listePersonnes(Request $request)
     {
         $page = $request->query->get('page');
         $limit = $request->query->get('limit');
         $data = array("page" => $page, "personnes" => array());
         
         $em = $this->getDoctrine()->getManager();
         $query = $em->createQuery('
           SELECT p FROM AppBundle:Personnes p
           WHERE p.actif = TRUE
           ORDER BY p.nom, p.prenom
         ');
         $query->setMaxResults($limit);
         $query->setFirstResult($limit*$page);
        
         //Ajouter Statut (coach, entraineur, marqueur) et équipe (Golf mixte, Basketball div 2 M, et)c
         foreach ($query->getResult() as $p){
            $data["personnes"][] =  array(
                "id" => $p->getId(),
                "nom_complet" => $p->getNomComplet(),
                "courriel" => $p->getCourriel(),
                "no_tel" => $p->getNoTel(),
            );
         }
         
         return new JsonResponse($data);
     }

    // Lie les roles avec une personne
    private function bindRoles($personne, $roles){
      $em = $this->getDoctrine()->getManager();
      
      // Supprime tous les rôles existants de la personne
      $query = $em->createQuery('
       DELETE FROM AppBundle:PersonneRoles pr 
       WHERE pr.personne = :id'
      )->setParameter('id', $personne->getId());
      $query = $query->getSingleScalarResult();
      
      // Ajoute les nouveaux rôles
      $query = $em->createQuery('
       SELECT r FROM AppBundle:Roles r
       WHERE r.id IN (:roles_array)'
      )->setParameter('roles_array', $roles);
      
      foreach ($query->getResult() as $role){
        $pr = new PersonneRoles();
        $pr->setPersonne($personne);
        $pr->setRole($role);
        $em->persist($pr);
      } 
      $em->flush();
    }
    
    private function listRoles(&$vars, $personne){
      $em = $this->getDoctrine()->getManager();
      $roles = [];
      $selected_roles = [];
        
      // Tous les roles actifs
      $query = $em->createQuery('SELECT r FROM AppBundle:Roles r WHERE r.actif = TRUE ORDER BY r.nom');
      foreach ($query->getResult() as $r){ $roles[$r->getNom()] = $r->getId(); }
      
      // Roles sélectionnés si la personne existe déjà
      if($personne->getId() !== null){
        $query = $em->createQuery('
          SELECT pr, r FROM AppBundle:PersonneRoles pr
          JOIN pr.role r WHERE pr.personne = :id'
        ) -> setParameter('id', $personne->getId());
        
        foreach ($query->getResult() as $pr){ 
          $role = $pr->getRole();
          $rnom = $role->getNom();
          $rid = $role->getId();
          $selected_roles[$rnom] = $rid; 
          
          // Ajoute les rôles désactivés (mais sélectionné antérieurement) à la liste de rôles.
          if(!array_key_exists($rnom, $roles)){ $roles[$rnom] = $rid;  }
        }
      }
      
      $vars["roles"] = $roles;
      $vars["selected_roles"] = $selected_roles;
    }
    

    private function personnesForm($personne, $submit_label){
        $sexe_list = array('Homme ' => 'M', 'Femme' => 'F');

        $form = $this->createFormBuilder($personne)
        ->add('nom', TextType::class, array('label' => 'Nom', 'error_bubbling' => true))
        ->add('prenom', TextType::class, array('label' => 'Prenom', 'error_bubbling' => true))
        ->add('courriel', TextType::class, array('label' => 'Adresse courriel', 'error_bubbling' => true, 'required' => false))
        ->add('noTel', TextType::class, array('label' => 'Numéros de téléphones', 'error_bubbling' => true, 'required' => false ))
        ->add('dateNaissance', TextType::class, array('label' => 'Date Naissance', 'error_bubbling' => true, 'required' => false))
        ->add('sexe', ChoiceType::class, array( 
            'choices'  => $sexe_list,
            'choices_as_values' => true,
            'expanded' => true,
            'multiple' => false,
            'label' => "Sexe"))
        ->add('note', TextareaType::class, array('label' => 'Note', 'required'=>false, 'error_bubbling' => true))
        ->add('auth_embauche', TextType::class, array('label' => 'Authorization d\'embauches', 'required'=>false, 'error_bubbling' => true))
        ->add('save', SubmitType::class, array('label' => $submit_label))
        ->getForm();
       return $form;
    }

}
