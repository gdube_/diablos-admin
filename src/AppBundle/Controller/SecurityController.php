<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

use AppBundle\Entity\Utilisateurs;

class SecurityController extends Controller
{
  
  /**
   * @Route("/login", name="login")
   */
  public function loginAction(Request $request)
  {
    $em = $this->getDoctrine()->getManager();
    $admin_count = $em->createQuery('
     SELECT COUNT(u.id) FROM AppBundle:Utilisateurs u
     WHERE u.niveau_acces = \'ROLE_ADMIN\'
    ')->getSingleScalarResult();

    // Si il n'y a pas d'admin, génère le compte par défaut
    if(!$admin_count){
      $admin = new Utilisateurs();
      $admin->setNomUtilisateur("admin")
       ->setMotDePasse("admin")
       ->setNiveauAcces("ROLE_ADMIN");
      $em->persist($admin);
      $em->flush();
    }
    
    return $this->render('default/login.html.twig', array("last_username" => ""));
  }
  
  /**
   * @Route("/login_check", name="login_check")
   */
  public function loginCheckAction(Request $request)
  {
    $authenticationUtils = $this->get('security.authentication_utils');
    $error = $authenticationUtils->getLastAuthenticationError();
    $lastUsername = $authenticationUtils->getLastUsername();
    
    $vars = array('last_username' => $lastUsername, 'error' => $error);
    return $this->render('/', $vars);
  }
    
}