<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Activites;
use AppBundle\Entity\ActivitePersonneRoles;
use AppBundle\Form\Type\TransportType;
use AppBundle\Form\Type\ReservationHotelType;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

use Symfony\Component\Form\FormError;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class ActivitesController extends Controller
{

     /**
     * @Route("/activites", name="activites-index")
     */
    public function indexAction(Request $request)
    {
        $count = $this->getDoctrine()->getManager()->createQuery('SELECT COUNT(a.id) FROM AppBundle:Activites a')->getSingleScalarResult();
        $limit = 20;
        $page = 0;
        $max_page = round($count/$limit, 0, PHP_ROUND_HALF_UP);
        $vars = array("limit" => $limit, "page" => $page, "max_page"=>$max_page);
        
        $equipes = [];

        $em = $this->getDoctrine()->getManager();
        $query = $em->createQuery(
          'SELECT e, s FROM AppBundle:Equipes e JOIN e.sport s
           WHERE e.actif = TRUE'
        );
        
        foreach ($query->getResult() as $e){
          $equipes[$e->getNomComplet()] = $e->getId();
        }


        $vars = array("limit" => $limit, "page" => $page, "max_page"=>$max_page, "equipes"=>$equipes);

        $errors = $request->query->get("extra_errors");
        if(isset($errors)) $vars["extra_errors"] = $errors; 
        
        $flash = $request->query->get("flash");
        if(isset($flash)) $vars["flash"] = $flash; 
        
        $vars["user"] = $this->getUser();
        
        return $this->render('activites/index.html.twig', $vars);
    }

    /**
     * @Route("/activites/new", name="activites-new")
     */
    public function newAction(Request $request)
    {
        $activite = new Activites();
        
        $def_date = $request->query->get("date");
        if(isset($def_date))
          $activite->setDateHeure($def_date);
        
        $em = $this->getDoctrine()->getManager();
        $form = $this->activitesForm($activite, "Nouvelle activité");
        
        $form->handleRequest($request);
        $this->validateForm($form);
        
        if ($form->isSubmitted() && $form->isValid()) {
            $calendar = $request->request->get('calendar');
            $equipe_id = $request->request->get('form')["equipe"];

            $activite = $form->getData();
            $activite->setEquipe($this->getDoctrine()->getRepository("AppBundle:Equipes")->find($equipe_id));

            $transport = $activite->getTransport();
            $activite->setTransport(null);
            
            $reserv = $activite->getReservationHotel();
            $activite->setReservationHotel(null);

            // Sauvegarder sur la BD
            $em = $this->getDoctrine()->getManager();
            $em->persist($activite);
            $em->persist($transport->setActivite($activite));
            $em->persist($reserv->setActivite($activite));
            $em->flush();
            $this->applyRoles($activite, $request->request);
            return $this->calendatRedirect($calendar, "Nouvelle activité créée", $activite->getId());
        }

        $vars = array('form' => $form->createView(), 'content_title' => "Nouvelle Activité",
         'create_mode' => true, 'calendar' => $this->checkCalendar($request), 'user' => $this->getUser());    
        $this->setupRoles($vars, $activite);
        return $this->render('activites/form.html.twig', $vars);
    }

    /**
     * @Route("/activites/{id}/report", name="activites-show")
     */
    public function showAction(Request $request, $id)
    {
      $em = $this->getDoctrine()->getManager();
      $query = $em->createQuery(
        'SELECT a, e, s, t, h FROM AppBundle:Activites a JOIN a.equipe e 
         JOIN e.sport s JOIN a.transport t JOIN a.reservationHotel h
         WHERE a.id = :id'
      )->setParameter('id', $id);
      
      $activite = $query->setMaxResults(1)->getOneOrNullResult();
      if (!$activite) {
          $errors = array("L'activité n'existe pas.");
          return $this->redirectToRoute('activites-index', array("extra_errors" => $errors));
      }
      
      $vars = array("activite" => $activite, 'user' => $this->getUser());
      $this->setupRoles($vars, $activite); 
      return $this->render('activites/resume.html.twig', $vars);
    }
    
    /**
     * @Route("/activites/{id}/print", name="activites-print")
     */
    public function printAction(Request $request, $id)
    {
      $em = $this->getDoctrine()->getManager();
      $query = $em->createQuery(
        'SELECT a, e, s, t, h FROM AppBundle:Activites a JOIN a.equipe e 
         JOIN e.sport s JOIN a.transport t JOIN a.reservationHotel h
         WHERE a.id = :id'
      )->setParameter('id', $id);
      
      $activite = $query->setMaxResults(1)->getOneOrNullResult();
      if (!$activite) {
          $errors = array("L'activité n'existe pas.");
          return $this->redirectToRoute('activites-index', array("extra_errors" => $errors));
      }
      
      $vars = array("activite" => $activite, 'user' => $this->getUser());
      $this->setupRoles($vars, $activite); 
      
      $html = $this->renderView('activites/print.html.twig', $vars);
      return new Response(
          $this->get('knp_snappy.pdf')->getOutputFromHtml($html),
          200,
          array(
              'Content-Type'          => 'application/pdf',
              'Content-Disposition'   => 'attachment; filename="activite_'. $activite->getId() .'"'
          )
      );
    }

    /**
     * @Route("/activites/{id}", name="activites-edit")
     */
    public function editAction(Request $request, $id)
    {
      $user = $this->getUser();
      if(!in_array($user->getNiveauAcces(), array("ROLE_ADMIN", "ROLE_EDITOR"))){
        // Redirige l'utilisateur si il n'a pas acces à l'edit
        return $this->redirectToRoute('activites-show', array("id"=>$id));
      }
      
      $em = $this->getDoctrine()->getManager();
      $query = $em->createQuery(
        'SELECT a, e, s, t, h FROM AppBundle:Activites a JOIN a.equipe e 
         JOIN e.sport s JOIN a.transport t JOIN a.reservationHotel h
         WHERE a.id = :id'
      )->setParameter('id', $id);
      
      $activite = $query->setMaxResults(1)->getOneOrNullResult();
      if (!$activite) {
          $errors = array("L'activité n'existe pas.");
          return $this->redirectToRoute('activites-index', array("extra_errors" => $errors));
      }
      
      $form = $this->activitesForm($activite, "Modifier l'activité");
      $form->handleRequest($request);
      $this->validateForm($form);
      
      if ($form->isSubmitted() && $form->isValid()) {
        $calendar = $request->request->get('calendar');
        $equipe_id = $request->request->get('form')["equipe"];
        $activite = $form->getData();
        $activite->setEquipe($this->getDoctrine()->getRepository("AppBundle:Equipes")->find($equipe_id));
        $em->flush();
        $this->applyRoles($activite, $request->request);
        $date = $activite->getDateHeureTraduit();
        return $this->calendatRedirect($calendar, "L'activité du ".$date." a été modifié", $activite->getId());
      }
      
      $vars = array('form' => $form->createView(), 'content_title' => "Modifier une activité",
      'calendar' => $this->checkCalendar($request), 'user' => $user); 
      $this->setupRoles($vars, $activite);   
      return $this->render('activites/form.html.twig', $vars);
    }
    
   /**
    * @Route("/activites/{id}/delete.json", name="activites-delete")
    */
    public function deleteAction(Request $request, $id)
    {
      $em = $this->getDoctrine()->getManager();
      $query = $em->createQuery(
        'SELECT a FROM AppBundle:Activites a WHERE a.id = :id'
      )->setParameter('id', $id);
      
      $activite = $query->setMaxResults(1)->getOneOrNullResult();
      if (!$activite) {
        $res = new JsonResponse(array('error' => "L'activité n'existe pas"));
        $res->setStatusCode(404, null);
        return $res;
      }
      
      // Supprime les ActivitePersonneRoles
      $query = $em->createQuery(
        'SELECT apr FROM AppBundle:ActivitePersonneRoles apr WHERE apr.activite = :id'
      )->setParameter('id', $id);
      
      foreach($query->getResult() as $apr){
        $em->remove($apr);
      }
      
      // Supprime l'activité
      $em->remove($activite);
      $em->flush();
      
      $msg = "L'activité à la date '". $activite->getDateHeureTraduit() ."' a été supprimée";
      return new JsonResponse(array('success' => $msg ));
    }
    
    /**
     * @Route("/list_activites_calendar.json", name="liste_activites_calendar")
     */
    public function listeActivitesCalendar(Request $request){
      $data = array("date" => array());
      
      $em = $this->getDoctrine()->getManager();
      $query = $em->createQuery(
        'SELECT a, e, s FROM AppBundle:Activites a JOIN a.equipe e JOIN e.sport s
         ORDER BY a.dateHeure'
      );
      
      foreach ($query->getResult() as $a){
        $status = json_decode($a->getStatut(), true)["status"];
        
        $data["activites"][] =  array(
            "id" => $a->getId(),
            "date" => $a->getDateHeure(false),
            "equipe" => $a->getEquipe()->getNomAbrev(),
            "status" => $status,
            "division" => $a->getEquipe()->getNiveau()
        );
      }
      
      return new JsonResponse($data);
    }
      
    /**
     * @Route("/recherche_activites.json", name="liste_activites")
     */
     public function listeActivites(Request $request)
     {
         $page = $request->query->get('page');
         $limit = $request->query->get('limit');
         $data = array("page" => $page, "activites" => array());

         $equipe = $request->query->get('equipe');
         if(empty($equipe)) $equipe = 0;
         else $equipe = intval($equipe);
         
         $minDate = $request->query->get('minDate');
         if(empty($minDate)) $minDate = null;
         else $minDate = \DateTime::createFromFormat('Y/m/d H:i', $minDate);
        
         $maxDate = $request->query->get('maxDate');
         if(empty($maxDate)) $maxDate = null;
         else $maxDate = \DateTime::createFromFormat('Y/m/d H:i', $maxDate);
        
         $statut = $request->query->get('statut');
         if(!empty($statut)) $statut = "%".$statut."%";
         
         $rencontre = $request->query->get('rencontre');
         $receveur = $request->query->get('receveur');
         $visiteur = $request->query->get('visiteur');
         
         
         $param = 'WHERE (TRUE = :boolEquipe OR e.id = :equipe)
          AND (TRUE = :boolRecontre OR a.type LIKE :rencontre)
          AND (TRUE = :boolMinDate OR a.dateHeure > :minDate) 
          AND (TRUE = :boolMaxDate OR a.dateHeure < :maxDate)
          AND (TRUE = :boolReceveur OR a.receveur LIKE :receveur)
          AND (TRUE = :boolVisiteur OR a.visiteur LIKE :visiteur)
          AND (TRUE = :boolStatut OR a.statut LIKE :statut)';

         $em = $this->getDoctrine()->getManager();
         $query = $em->createQuery(
            'SELECT a, e, s FROM AppBundle:Activites a JOIN a.equipe e JOIN e.sport s ' . $param . ' ORDER BY a.dateHeure'
         )->setParameters(array(
            'equipe' => $equipe,
            'rencontre' => $rencontre,
            'minDate' => $minDate,
            'maxDate' => $maxDate,
            'receveur' => '%' . $receveur . '%',
            'visiteur' => '%' . $visiteur . '%',
            'statut' => $statut,
            'boolEquipe' => ($equipe == 0),
            'boolRecontre' => empty($rencontre),
            'boolMinDate' => empty($minDate),
            'boolMaxDate' => empty($maxDate),
            'boolReceveur' => empty($receveur),
            'boolVisiteur' => empty($visiteur),
            'boolStatut' => empty($statut)
         ));
         $query->setMaxResults($limit);
         $query->setFirstResult($limit*$page);

         foreach ($query->getResult() as $a){
            $e = $a->getEquipe();
            $data["activites"][] =  array(
                "id" => $a->getId(),
                "dateHeure" => $a->getDateHeureTraduit(),
                "type" => $a->getType(),
                "receveur" => $a->getReceveur(),
                "visiteur" => $a->getVisiteur(),
                "lieu" => $a->getLieu(),
                "equipe" => $e->getNomComplet()
            );
         }
         
         return new JsonResponse($data);
     }
     
     /*
      Vérifie si l'utilisateur à été redirigé par le calendrier
     */
     private function checkCalendar($request){
       $calendar = $request->query->get("calendar");
       if(isset($calendar)) $calendar = true;
       return $calendar;
     }
     
     /*
      Vérifie si l'utilisateur à été redirigé par le calendrier
     */
     private function calendatRedirect($calendar, $msg, $id){
       if ($calendar == True)
         return $this->redirectToRoute('homepage', array("flash" => $msg));
       else
         return $this->redirectToRoute('activites-index', array("id"=> $id ,"flash" => $msg));
     }
     
     /*
      Valide la form de l'activité
     */
     private function validateForm($form){
       if ($form->isSubmitted()) {
          $activite = $form->getData();
          $transport = $activite->getTransport();
          $hotel = $activite->getReservationHotel();
          
          //Check Equipe
          if( $activite->getEquipe() === null )
            $form->addError(new FormError('Il doit y avoir une équipe d\'associée'));
          
          // Check date transport
          $d0 = $activite->getDateHeure();
          $d1 = $transport->getHeureDepart();
          $d2 = $transport->getHeureRetour();
          $d3 = $transport->getDateReservation();
          $d4 = $hotel->getHeureDepart();
          $d5 = $hotel->getHeureRetour();

          if(isset($d1) && isset($d2) && !empty($d1) && !empty($d2))
            if($d1 >= $d2) $form->addError(new FormError('La date de départ ne peut pas être après la date de retour (transport)'));
          
          if( isset($d0) && isset($d1) && !empty($d1))
            if($d1 >= $d0) $form->addError(new FormError('La date de départ ne peut pas être après de début de l\'activité (transport)'));
          
          if( isset($d0) && isset($d2) && !empty($d2)) 
            if($d2 <= $d0) $form->addError(new FormError('La date de retour ne peut pas être avant de début de l\'activité (transport)'));
         
          if( isset($d0) && isset($d3) && !empty($d3))    
            if($d3 >= $d0) $form->addError(new FormError('La date de réservation ne peut pas être après de début de l\'activité (transport)'));

          if( isset($d4) && isset($d5) && !empty($d4)  && !empty($d5))    
            if($d4 >= $d5) $form->addError(new FormError('La date de départ ne peut pas être après la date de retour (hotel)'));

       }
     }  
     
     /*
      Créer la form de l'activité
     */
     private function activitesForm($activite, $submit_label){
         $equipes = [];
         $nom_equipe = "";

         $em = $this->getDoctrine()->getManager();
         $query = $em->createQuery(
           'SELECT e, s FROM AppBundle:Equipes e JOIN e.sport s
            WHERE e.actif = TRUE
            ORDER BY s.sport'
         );
         
         foreach ($query->getResult() as $e){
            $equipes[$e->getNomComplet()] = $e->getId();
         }
         
         $current_equipe = $activite->getEquipe();
         if(isset($current_equipe)){
             if($current_equipe->getActif() == false)
                 $equipes[$current_equipe->getNomComplet()] = $current_equipe->getId();
                 
             $nom_equipe = $current_equipe->getId();
         }else if(count($equipes) > 0){
             $nom_equipe = array_values($equipes)[0];
         }
         
         $form = $this->createFormBuilder($activite)
            ->add('type', ChoiceType::class, array(
                'error_bubbling' => true,
                'choices' => array(
                    'Match' => 'Match',
                    'Tournoi' => 'Tournoi',
                    'Hors saison' => 'Hors saison',
                    ),
                'choices_as_values' => true,
                'label' => 'Type d\'activité'))
            ->add('equipe', ChoiceType::class, array(
                'error_bubbling' => true,
                'choices' => $equipes,
                'choices_as_values' => true,
                'label' => 'Équipe',
                'data' => $nom_equipe))   // Important! Si on ne donne pas de data, symfony va essayer de caster l'equipe en string (et sa va planter)
            ->add('lieu', TextType::class, array('error_bubbling' => true))
            ->add('receveur', TextType::class, array('label' => 'Receveur',  'error_bubbling' => true))
            ->add('visiteur', TextType::class, array('label' => 'Visiteur',  'error_bubbling' => true))
            ->add('dateHeure', TextType::class, array('label' => 'Date et heure',  'error_bubbling' => true))
            ->add('statut', TextType::class, array('label' => 'Statut',  'error_bubbling' => true))
            ->add('transport', TransportType::class)
            ->add('reservationHotel', ReservationHotelType::class)
            ->add('save', SubmitType::class, array('label' => $submit_label))
            ->getForm();
         return $form;
     }
     
     /*
      Applique les rôles sélectionné à l'activité
     */
     private function applyRoles($activite, $request){
       $em = $this->getDoctrine()->getManager();
       
       // Supprime tous les rôles existants de l'activité
       $query = $em->createQuery('
        DELETE FROM AppBundle:ActivitePersonneRoles apr 
        WHERE apr.activite = :id'
       )->setParameter('id', $activite->getId());
       $query = $query->getSingleScalarResult();
       
       // Charge tous les utilisateur
       $pers = [];
       $query = $em->createQuery('SELECT p FROM AppBundle:Personnes p');
       foreach ($query->getResult() as $p){ $pers[$p->getId()] = $p; }
       
       // Ajoute les nouveaux rôles
       $logger = $this->get('logger');
       $query = $em->createQuery('SELECT r FROM AppBundle:Roles r');
       foreach ($query->getResult() as $r){
         $personnes = $request->get('roles_'.$r->getId(), []);
         if(!empty($personnes)){
           foreach ($personnes as $p_id){
             $apr = new ActivitePersonneRoles();
             $apr->setPersonne($pers[$p_id]);
             $apr->setRole($r);
             $apr->setActivite($activite);
             $em->persist($apr);
           }
         }
       }
       $em->flush();
     }

     /*
      Créer les roles de l'activité
     */
     private function setupRoles(&$vars, $activite){
       $em = $this->getDoctrine()->getManager();
       $roles = [];
       $personnes_roles = [];
       $selected_personnes_roles = [];
       
       // Tous les roles actifs
       $query = $em->createQuery('SELECT r FROM AppBundle:Roles r WHERE r.actif = TRUE ORDER BY r.nom');
       foreach ($query->getResult() as $r){ 
         $roles[$r->getId()] = $r->getNom();
         $personnes_roles[$r->getId()] = [];
         $selected_personnes_roles[$r->getId()] = [];
       }
       
       // Charge toutes les personnes active qui ont des rôles actifs
       $query = $em->createQuery('
        SELECT pr, p, r FROM AppBundle:PersonneRoles pr JOIN pr.personne p JOIN pr.role r
        WHERE p.actif = TRUE and r.actif = TRUE
        ORDER BY p.nom, p.prenom'
       );
       
       // Map les roles avec les personnes
       foreach ($query->getResult() as $pr){
         $r = $pr->getRole()->getId();
         $p = $pr->getPersonne();
         $personnes_roles[$r][$p->getId()] = $p->getNomComplet();
       }
     
       // Charge toutes les personnes qui ont des rôles associé a cette activité
       if( $activite->getId() !== null ){
         $query = $em->createQuery('
          SELECT apr, p, r FROM AppBundle:ActivitePersonneRoles apr JOIN apr.personne p JOIN apr.role r
          WHERE apr.activite = :id
         ')->setParameter('id', $activite->getId());
         
         // Map les rôles avec les personnes
         foreach ($query->getResult() as $apr){
           $r = $apr->getRole();
           $p = $apr->getPersonne();
           $r_id = $r->getId();
           $p_id = $p->getId();
           
           // Ajoute les rôles inactifs au besoin
           if(!array_key_exists($r_id, $selected_personnes_roles)){
             $roles[$r_id] = $r->getNom() . " (Inactif)";
             $selected_personnes_roles[$r_id] = [];
             $personnes_roles[$r_id] = [];
           }
           
           // Ajoute les personnes inactives au besoin
           if(!array_key_exists($p_id, $personnes_roles[$r_id] )){
             if($p->getActif())
               $personnes_roles[$r_id][$p_id] = $p->getNomComplet();
             else
               $personnes_roles[$r_id][$p_id] = $p->getNomComplet()." (Inactif)";
           }
             
           $selected_personnes_roles[$r_id][$p_id] = $p->getNomComplet();
         }
       }
     
       //Regroupe les roles par groupe de 3
       $i = 0;
       $arr = [];
       $role_groups = [];
       
       foreach($roles as $r_id => $r_nom){
         $arr[$r_id] = $r_nom;
         if(count($arr) == 3){
           array_push($role_groups, $arr);
           $arr = [];
         }
       }
       if(!empty($arr))
        array_push($role_groups, $arr);
       
       $vars["roles_group"] = $role_groups;
       $vars["personnes_roles"] = $personnes_roles;
       $vars["selected_personnes_roles"] = $selected_personnes_roles;
     }
}