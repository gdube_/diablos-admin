<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

use AppBundle\Entity\Roles;

class RolesController extends Controller
{
    /**
     * @Route("/roles", name="roles-index")
     */
    public function indexAction(Request $request)
    {
        $count = $this->getDoctrine()->getManager()->createQuery('SELECT COUNT(r.id) FROM AppBundle:Roles r')->getSingleScalarResult();
        $limit = 20;
        $page = 0;
        $max_page = ceil($count/$limit);
        $vars = array("limit" => $limit, "page" => $page, "max_page"=>$max_page);
        
        $errors = $request->query->get("extra_errors");
        if(isset($errors)) $vars["extra_errors"] = $errors; 
        
        $flash = $request->query->get("flash");
        if(isset($flash)) $vars["flash"] = $flash; 
        
        $vars["user"] = $this->getUser();
        
        return $this->render('roles/index.html.twig', $vars);
    }
    
    /**
     * @Route("/roles/new", name="role-new")
     */
    public function newAction(Request $request)
    {
        $role = new Roles();
        $role->setActif(true);
        $form = $this->rolesForm($role, "Nouveau Role");
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
         
          $em = $this->getDoctrine()->getManager(); 
          $em->persist($form->getData());
          $em->flush();
          
          return $this->redirectToRoute('roles-index', array("flash" => "Le rôle a été créé"));
        }
            
        $vars = array('form' => $form->createView(), 'content_title' => "Nouveau Rôle", 'user' => $this->getUser());    
        
        return $this->render('roles/form.html.twig', $vars);
    }
   
   /**
     * @Route("/roles/{id}", name="role-edit")
     */
    public function editAction(Request $request, $id)
    {
      $em = $this->getDoctrine()->getManager();
      $query = $em->createQuery(
        'SELECT r FROM AppBundle:Roles r WHERE r.id = :id'
      )->setParameter('id', $id);
      
      $role = $query->setMaxResults(1)->getOneOrNullResult();
      if (!$role) {
          $errors = array("Le rôle n'existe pas.");
          return $this->redirectToRoute('roles-index', array("extra_errors" => $errors));
      }
      
      $form = $this->rolesForm($role, "Modifier le rôle");
      $form->handleRequest($request);
      if ($form->isSubmitted() && $form->isValid()) {
        $em = $this->getDoctrine()->getManager();
        $role = $form->getData();
        $em->flush();
        return $this->redirectToRoute('roles-index', array("flash" => "Le rôle '".$role->getNom()."' a été modifié"));
      }
      
      $vars = array('form' => $form->createView(), 'content_title' => "Modifier un rôle", 'user' => $this->getUser());    
      return $this->render('roles/form.html.twig', $vars);
    }
    
    /**
    * @Route("/roles/{id}/delete.json", name="roles-delete")
    */
    public function deleteAction(Request $request, $id)
    {
      $em = $this->getDoctrine()->getManager();
      $query = $em->createQuery(
        'SELECT r FROM AppBundle:Roles r WHERE r.id = :id'
      )->setParameter('id', $id);

      $role = $query->setMaxResults(1)->getOneOrNullResult();
      if(!$role){
        $res = new JsonResponse(array('error' => "Le rôle n'existe pas"));
        $res->setStatusCode(404, null);
        return $res;
      }

      $role->setActif(false);
      $em->flush();
      
      $msg = "Le rôle '". $role->getNom() ."' a été désactivé";
      return new JsonResponse(array('success' => $msg ));
    }
   
   /**
    * @Route("/roles-recherche.json", name="liste-roles")
    */
    public function listeRoles(Request $request)
    {
        $page = $request->query->get('page');
        $limit = $request->query->get('limit');
        $data = array("page" => $page, "roles" => array());
        
        $em = $this->getDoctrine()->getManager();
        $query = $em->createQuery('
          SELECT r FROM AppBundle:Roles r
          WHERE r.actif = TRUE
          ORDER BY r.nom
        ');
        $query->setMaxResults($limit);
        $query->setFirstResult($limit*$page);
      
        foreach ($query->getResult() as $e){
          $data["roles"][] = array(
            "nom" => $e->getNom(),
            "id" => $e->getId()
            );
        }
        
        return new JsonResponse($data);
    }
    
    private function rolesForm($role, $submit_label){

      $form = $this->createFormBuilder($role)
          ->add('nom', TextType::class, array('label' => 'Nom', 'error_bubbling' => true))
          ->add('save', SubmitType::class, array('label' => $submit_label))
          ->getForm();
      return $form;
    }
    
}

