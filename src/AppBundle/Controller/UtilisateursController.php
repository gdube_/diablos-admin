<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

use AppBundle\Entity\Utilisateurs;

class UtilisateursController extends Controller
{
    /**
     * @Route("/utilisateurs", name="utilisateurs-index")
     */
    public function indexAction(Request $request)
    {
        $count = $this->getDoctrine()->getManager()->createQuery('SELECT COUNT(r.id) FROM AppBundle:Utilisateurs r')->getSingleScalarResult();
        $limit = 20;
        $page = 0;
        $max_page = ceil($count/$limit);
        $vars = array("limit" => $limit, "page" => $page, "max_page"=>$max_page);
        
        $errors = $request->query->get("extra_errors");
        if(isset($errors)) $vars["extra_errors"] = $errors; 
        
        $flash = $request->query->get("flash");
        if(isset($flash)) $vars["flash"] = $flash; 
        
        $vars["user"] = $this->getUser();
        
        return $this->render('utilisateurs/index.html.twig', $vars);
    }
    
    /**
     * @Route("/utilisateurs/new", name="utilisateur-new")
     */
    public function newAction(Request $request)
    {
        $role = new Utilisateurs();
        $form = $this->utilisateursForm($role, "Nouvel Utilisateur");
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
         
          $em = $this->getDoctrine()->getManager(); 
          $em->persist($form->getData());
          $em->flush();
          
          return $this->redirectToRoute('utilisateurs-index', array("flash" => "L'utilisateur a été créé"));
        }
            
        $vars = array('form' => $form->createView(), 'content_title' => "Nouvel Utilisateur", 'user' => $this->getUser());    
        
        return $this->render('utilisateurs/form.html.twig', $vars);
    }
    
    /**
     * @Route("/utilisateurs/{id}", name="utilisateur-edit")
     */
    public function editAction(Request $request, $id)
    {
      $em = $this->getDoctrine()->getManager();
      $query = $em->createQuery(
        'SELECT u FROM AppBundle:Utilisateurs u WHERE u.id = :id'
      )->setParameter('id', $id);
      
      $utilisateur = $query->setMaxResults(1)->getOneOrNullResult();
      if (!$utilisateur) {
          $errors = array("L'utilisateur n'existe pas.");
          return $this->redirectToRoute('utilisateurs-index', array("extra_errors" => $errors));
      }
      
      $form = $this->utilisateursForm($utilisateur, "Modifier l'utilisateur");
      $form->handleRequest($request);
      if ($form->isSubmitted() && $form->isValid()) {
        $em = $this->getDoctrine()->getManager();
        $role = $form->getData();
        $em->flush();
        return $this->redirectToRoute('utilisateurs-index', array("flash" => "L'utilisateur '".$utilisateur->getNomUtilisateur()."' a été modifié"));
      }
      
      $vars = array('form' => $form->createView(), 'content_title' => "Modifier un utilisateur", 'user' => $this->getUser());    
      return $this->render('utilisateurs/form.html.twig', $vars);
    }
    
    /**
    * @Route("/utilisateurs/{id}/delete.json", name="utilisateur-delete")
    */
    public function deleteAction(Request $request, $id)
    {
      $em = $this->getDoctrine()->getManager();
      $query = $em->createQuery(
        'SELECT u FROM AppBundle:Utilisateurs u WHERE u.id = :id'
      )->setParameter('id', $id);

      $utilisateur = $query->setMaxResults(1)->getOneOrNullResult();
      if(!$utilisateur){
        $res = new JsonResponse(array('error' => "L'utilisateur n'existe pas"));
        $res->setStatusCode(404, null);
        return $res;
      }

      $utilisateur->setNiveauAcces("ROLE_DISABLED");
      $em->flush();
      
      $msg = "L'utilisateur '". $utilisateur->getNomUtilisateur() ."' a été désactivé";
      return new JsonResponse(array('success' => $msg ));
    }
    
    /**
    * @Route("/utilisateurs-recherche.json", name="liste-utilisateurs")
    */
    public function listeUtilisateurs(Request $request)
    {
        $page = $request->query->get('page');
        $limit = $request->query->get('limit');
        $data = array("page" => $page, "roles" => array());
        
        $em = $this->getDoctrine()->getManager();
        $query = $em->createQuery('
          SELECT u FROM AppBundle:Utilisateurs u
          WHERE u.niveau_acces != \'ROLE_DISABLED\'
          ORDER BY u.nom_utilisateur
        ');
        $query->setMaxResults($limit);
        $query->setFirstResult($limit*$page);
      
        $reverse_access = array("ROLE_ADMIN" => "Administrateur", "ROLE_EDITOR" => "Éditeur", "ROLE_USER" => "Lecture", "ROLE_DISABLED" => "Inactif");
      
        foreach ($query->getResult() as $e){
          $data["utilisateurs"][] = array(
            "nom_utilisateur" => $e->getNomUtilisateur(),
            "access" => $reverse_access[$e->getNiveauAcces()],
            "id" => $e->getId()
            );
        }
        
        return new JsonResponse($data);
    }
    
    
    private function utilisateursForm($utilisateur, $submit_label){
      $acces_list = array('Admin' => 'ROLE_ADMIN', 'Éditeur' => 'ROLE_EDITOR', 'Lecture' => 'ROLE_USER', 'Inactif' => 'ROLE_DISABLED');
      
      $form = $this->createFormBuilder($utilisateur)
          ->add('nom_utilisateur', TextType::class, array('label' => 'Nom d\'utilisateur', 'error_bubbling' => true))
          ->add('mot_de_passe', TextType::class, array('label' => 'Mot de passe', 'error_bubbling' => true))
          ->add('niveau_acces', ChoiceType::class,
            array( 'choices'  => $acces_list, 
                   'choices_as_values' => true,
                   'expanded' => false,
                   'multiple' => false,
                   'label' => "Droits d'accès")
                 )
          ->add('save', SubmitType::class, array('label' => $submit_label))
          ->getForm();
      return $form;
    }

}

