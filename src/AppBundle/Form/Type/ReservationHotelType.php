<?php
namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class ReservationHotelType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('hotel', TextType::class, array('label' => 'Nom de l\'hotel', 'error_bubbling' => true, 'required' => false));
        $builder->add('contact', TextType::class, array('label' => 'Contact (Téléphone)', 'error_bubbling' => true, 'required' => false));
        $builder->add('demandeAchat', TextType::class, array('label' => 'Demande d\'achat', 'error_bubbling' => true, 'required' => false));
        $builder->add('adresse', TextType::class, array('label' => 'Adresse', 'error_bubbling' => true, 'required' => false));
        $builder->add('note', TextareaType::class, array('label' => 'Note', 'error_bubbling' => true, 'required' => false));
        $builder->add('nombreChambre', IntegerType::class, array('label' => 'Nombre de chambres', 'error_bubbling' => true, 'required' => false));
        $builder->add('nombreNuit', IntegerType::class, array('label' => 'Nombre de nuits', 'error_bubbling' => true, 'required' => false));
        $builder->add('heureDepart', TextType::class, array('label' => 'Heure de départ', 'error_bubbling' => true, 'required' => false));
        $builder->add('heureRetour', TextType::class, array('label' => 'Heure de retour', 'error_bubbling' => true, 'required' => false));
        $builder->add('dateReservation', TextType::class, array('label' => 'Date de réservation', 'error_bubbling' => true, 'required' => false));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\ReservationsHotels',
        ));
    }
}