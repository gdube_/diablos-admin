<?php
namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class PersonneType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('nom', TextType::class, array('label' => 'Nom', 'error_bubbling' => true));
        $builder->add('prenom', TextType::class, array('label' => 'Prenom', 'error_bubbling' => true));
        $builder->add('courriel', TextType::class, array('label' => 'Adresse courriel', 'error_bubbling' => true));
        $builder->add('noTel', TextType::class, array('label' => 'Numéro de téléphone', 'error_bubbling' => true));
        $builder->add('dateNaissance', TextType::class, array('label' => 'Date Naissance', 'error_bubbling' => true));

        $sexe_list = array('Homme ' => 'M', 'Femme' => 'F');
        
        $builder->add('sexe', ChoiceType::class,
            array( 'choices'  => $sexe_list, 'choices_as_values' => true, 'expanded' => true,
            'multiple' => false, 'label' => "Sexe"));
        $builder->add('note', TextareaType::class, array('label' => 'Note', 'required'=>false, 'error_bubbling' => true));

        
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Personnes',
        ));
    }
}